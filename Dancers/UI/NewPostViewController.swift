//
//  NewPostViewController.swift
//  Dancers
//
//  Created by MacOS on 28.06.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit

class NewPostViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var CloseView: UIView!
    @IBOutlet weak var SaveView: UIView!
    @IBOutlet weak var TextView: UITextView!
    @IBOutlet weak var SaveButton: UIButton!
    
    // MARK: Closures
    
    var closeHandler: (() -> Void)?
    var addedHandler: (() -> Void)?
    var oldEditerHeigth = CGFloat(0.0)
    
    // MARK: Properties
    
    var Club: FClub!
    let imagePicker = UIImagePickerController()
    
    // MARK: UIViewController methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        self.oldEditerHeigth = self.TextView.bounds.height
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        
        dismiss(animated: true, completion: { [unowned self] in
            
            /*!!!guard let imageController = self.storyboard!.instantiateViewController(withIdentifier: "processImageViewController") as? ProcessImageViewController else {
                Utils.showError("INTERNAL_ERROR_STRING".localized, above: self)
                return
            }*/
            
            let fixedImage = pickedImage.fixedOrientation()
            /*!!!imageController.image = fixedImage ?? pickedImage
            self.present(imageController, animated: true)
            
            imageController.closeHandler = { [unowned self] (image) in
                
                imageController.dismiss(animated: true, completion: nil)*/
                
                guard let image = /*image*/fixedImage else {
                    return
                }
                
                let attachment = NSTextAttachment()
                attachment.image = image
                //calculate new size.  (-20 because I want to have a litle space on the right of picture)
                let newImageWidth = CGFloat(70.0) // (self.TextView.bounds.size.width - 20 )
                let scale = newImageWidth/image.size.width
                let newImageHeight = image.size.height * scale
                //resize this
                attachment.bounds = CGRect.init(x: 0, y: 0, width: newImageWidth, height: newImageHeight)
                //put your NSTextAttachment into and attributedString
                let attString = NSAttributedString(attachment: attachment)
                //add this attributed string to the current position.
                var range = self.TextView.selectedRange
                self.TextView.textStorage.insert(attString, at: range.location)
                range.location += 1
                self.TextView.selectedRange = range
                
                self.refreshEditer()
            //!!!}
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        refreshEditer()
    }
    
    // MARK: Methods
    
    func refreshEditer() {
        self.TextView.sizeHeightToFitAttributedString(self.oldEditerHeigth, self.view.bounds.size.height - 150.0)
    }
    
    func hideKeyboard() {
        self.TextView.resignFirstResponder()
    }
    
    func getTextStringParts() -> [FTextPart] {
        var parts = [FTextPart]()

        let attributedString = self.TextView.attributedText ?? NSAttributedString()
        let range = NSMakeRange(0, attributedString.length)
        attributedString.enumerateAttributes(in: range, options: NSAttributedString.EnumerationOptions(rawValue: 0)) { (object, range, stop) in
            let textPart = FSerializer.deserialize(FTextPart.self, [
                "Location": range.location
                ,"Length": range.length
            ])!
            
            if object.keys.contains(NSAttributedString.Key.attachment) {
                if let attachment = object[NSAttributedString.Key.attachment] as? NSTextAttachment {
                    if let image = attachment.image {
                        textPart.Image.Image = image
                    } else if let image = attachment.image(forBounds: attachment.bounds, textContainer: nil, characterIndex: range.location) {
                        textPart.Image.Image = image
                    }
                }
            } else {
                let stringValue : String = attributedString.attributedSubstring(from: range).string
                if (!stringValue.trimmingCharacters(in: .whitespaces).isEmpty) {
                    textPart.Text = stringValue
                }
            }
            
            parts.append(textPart)
        }
        return parts
    }
    
    // MARK: Actions
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        print("keyboardWillShow")
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardFrame = keyboardSize.cgRectValue
        
        self.SaveView.frame.origin.y = self.view.bounds.height - keyboardFrame.height - self.SaveView.bounds.height - 40.0
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        print("keyboardWillHide")
        self.SaveView.frame.origin.y = self.view.bounds.height - self.SaveView.bounds.height - 40.0
    }
    
    @IBAction func tapClicked(_ recognizer: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.closeHandler?()
    }
    
    @IBAction func addMediaClicked(_ sender: Any) {
        
        self.imagePicker.allowsEditing = false
        
        Utils.showConfirm("SELECT_MEDIA_SOURCE_STRING".localized, yesText: "CAMERA_STRING".localized, cancelText: "PHOTO_LIBRARY_STRING".localized, above: self) { [unowned self] (camera) in
            
            #if targetEnvironment(simulator)
            self.imagePicker.sourceType = .photoLibrary //.library
            #else
            self.imagePicker.sourceType = camera ? .camera : .photoLibrary //.camera
            
            if camera {
                self.imagePicker.cameraDevice = .front
            }
            #endif
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        
        let me = FManager.manager().me!
        let parts = getTextStringParts()
        
        let feedEntry = FSerializer.deserialize(FFeedEntry.self, [
            "Owner": me.Phone
        ], FManager.newKey())!
        feedEntry.Text = parts
        
        let presenter = Utils.showProgressWithPersents(self)
        self.Club.saveEntry(feedEntry, progressHandler: { (progress) in
            presenter.0(progress)
        }) { (error) in
            Utils.hideProgress(self)
            if let error = error {
                Utils.showErrorIfNeeded(error)
            }
            else {
                self.addedHandler?()
            }
        }
    }
}

//
//  ErrorViewController.swift
//  Pocket.CEO
//
//  Created by MacOS on 25/06/2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import UIKit
import SwiftPopup

class InfoViewController: SwiftPopup {
    
    // MARK: Outlets
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var InfoLabel: UILabel!
    @IBOutlet weak var CloseView: UIView!
    @IBOutlet weak var CloseButton: UIButton!
    
    // MARK: Properties
    
    var info: String!
    var error: String!
    var closeText = "CLOSE_STRING".localized
    
    // MARK: Closures
    
    var closeHandler: (() -> Void)?
    
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.CloseView.layer.cornerRadius = 10.0
        self.CloseView.layer.borderWidth = 3.0
        self.CloseView.layer.borderColor = UIColor(red: 0.32, green: 0.6, blue: 0.69, alpha: 0.2).cgColor
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = .center
        
        let font = UIFont(name: "Roboto", size: 16.0)!
        let textColor = UIColor(red: 0.15, green: 0.2, blue: 0.22, alpha: 1)
        
        let attributes: [NSAttributedString.Key: Any] = [.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
        let infoAttributedString = NSAttributedString(string: self.info ?? self.error, attributes: attributes)
        
        self.InfoLabel.attributedText = infoAttributedString
        self.CloseButton.setTitle(self.closeText, for: .normal)
        
        if self.error != nil {
            self.ImageView.image = UIImage(named: "alert")
        }
    }
    
    // MARK: Methods
    
    // MARK: Actions
    
    @IBAction func closeClicked(_ sender: Any) {
        
        self.dismiss {
            self.closeHandler?()
        }
    }
}

class ConfirmViewController: SwiftPopup {
    
    // MARK: Outlets
    
    @IBOutlet weak var InfoLabel: UILabel!
    @IBOutlet weak var OkView: UIView!
    @IBOutlet weak var OkButton: UIButton!
    @IBOutlet weak var CancelView: UIView!
    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var CloseView: UIView!
    @IBOutlet weak var CloseButton: UIButton!
    
    // MARK: Properties
    
    var info: String!
    var okText = "YES_STRING".localized
    var cancelText = "CANCEL_STRING".localized
    
    // MARK: Closures
    
    var okHandler: (() -> Void)?
    var cancelHandler: (() -> Void)?
    
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.CloseView.makeShadow()
        
        self.CloseView.layer.cornerRadius = 23.0
        self.CloseButton.layer.cornerRadius = 21.0
        
        self.CancelView.layer.cornerRadius = 10.0
        self.CancelView.layer.borderWidth = 3.0
        self.CancelView.layer.borderColor = UIColor(red: 0.32, green: 0.6, blue: 0.69, alpha: 0.2).cgColor
        
        self.OkButton.layer.cornerRadius = 10.0
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = .center
        
        let font = UIFont(name: "Roboto", size: 16.0)!
        let textColor = UIColor(red: 0.15, green: 0.2, blue: 0.22, alpha: 1)
        
        let attributes: [NSAttributedString.Key: Any] = [.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
        let infoAttributedString = NSAttributedString(string: self.info, attributes: attributes)
        
        self.InfoLabel.attributedText = infoAttributedString
        
        self.InfoLabel.text = info
        self.OkButton.setTitle(self.okText, for: .normal)
        self.CancelButton.setTitle(self.cancelText, for: .normal)
        self.OkView.makeShadow()
        
        self.OkButton.titleLabel?.minimumScaleFactor = 0.3
//        self.OkButton.titleLabel?.numberOfLines = 0
        self.OkButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        self.CancelButton.titleLabel?.minimumScaleFactor = 0.3
//        self.CancelButton.titleLabel?.numberOfLines = 0
        self.CancelButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    // MARK: Methods
    
    // MARK: Actions
    
    @IBAction func okClicked(_ sender: Any) {
        
        self.dismiss { [unowned self] in
            self.okHandler?()
        }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        
        self.dismiss { [unowned self] in
            self.cancelHandler?()
        }
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        
        self.dismiss { [unowned self] in
        }
    }
}

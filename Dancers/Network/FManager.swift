//
//  FManager.swift
//  Pazitive
//
//  Created by Владимир on 19.06.2018.
//  Copyright © 2018 ciferton. All rights reserved.
//

import FirebaseUI
import SwiftyTimer

class FManager {
    
    // MARK: Properties
    
    var Countries: [String: FCountry] = [:]
    var profiles: [String: FProfile] = [:]
    var clubs: [String: FClub] = [:]
    var myPhone: String!
    let asyncManager = AsyncManager()
    var removingProfile = false
    
    private static let Manager = FManager()
    
    var me: FProfile! {
        get {
            
            guard let myPhone = self.myPhone else { return nil}
            return self.profiles.map({ $0.value }).first(where: { $0.Phone == myPhone })
        }
    }
    
    // MARK: Closures
    
    // MARK: singleton
    
    class func manager() -> FManager {
        return Manager
    }
    
    // MARK: Constructor
    
    func initilization(_ country: String? = nil, completed: (() -> Void)?) {
        
        self.removingProfile = false
        self.asyncManager.maximumParallels = 1
        self.getCountries { [unowned self] in
            self.getMe(country) { (p) in
                self.getClubs() {
                    completed?()
                }
            }
        }
    }
    
    // MARK: Static
    
    class func newKey() -> String {
        
        let random = UUID().uuidString
        let ref = Database.database().reference().child(random)
        return ref.childByAutoId().key!
    }
    
    // MARK: methods
    
    func getProfiles(_ phones: [String], completed: (([FProfile]) -> Void)?) {
        
        let set1 = Set(phones)
        let set2 = Set(self.profiles.map({ $0.value.Phone }))
        guard let me = FManager.manager().me else {
            return
        }
        
        let subSet = set1.subtracting(set2)
        if subSet.count > 0 {
            
            let ref = Database.database().reference()
            let query = ref.child("Profiles").queryOrdered(byChild: "Phone")
            
            var count = 0
            for p in subSet {
                
                query.queryEqual(toValue: p).observeSingleEvent(of: .value) { [unowned self] (snapshot) in
                    
                    let enumerator = snapshot.children
                    while let rest = enumerator.nextObject() as? DataSnapshot {
                        
                        if let result = rest.value as? [String: Any] {
                            
                            let profile = FSerializer.deserialize(FProfile.self, result, p)!
                            self.profiles[profile.Phone] = profile
                            
                            if profile.Phone == me.Phone {
                                if count == subSet.count {
                                    completed?(self.profiles.filter({ phones.contains($0.value.Phone) }).map({ $0.value }).sorted(by: { (first, second) -> Bool in
                                        first.DisplayName < second.DisplayName
                                    }))
                                }
                            }
                        }
                    }
                    
                    count += 1
                    if count == subSet.count {
                        completed?(self.profiles.filter({ phones.contains($0.value.Phone) }).map({ $0.value }).sorted(by: { (first, second) -> Bool in
                            first.DisplayName < second.DisplayName
                        }))
                    }
                }
                
                query.queryEqual(toValue: p).observeSingleEvent(of: .childRemoved) { (snapshot) in
                    
                    self.profiles.removeValue(forKey: snapshot.key)
                    Utils.appDelegate().becomeView?.become([FProfile.self], nil)
                }
                
                query.queryEqual(toValue: p).observe(.childChanged) { (snapshot) in
                    
                    if let result = snapshot.value as? [String: Any] {
                        
                        let profile = FSerializer.deserialize(FProfile.self, result, p)!
                        self.profiles[profile.Phone] = profile
                        
                        Utils.appDelegate().becomeView?.become([FProfile.self], nil)
                    }
                }
            }
            
        }
        else {
            
            let result = self.profiles.filter({
                phones.contains($0.value.Phone)
            }).map({ $0.value }).sorted { (first, second) -> Bool in
                first.DisplayName < second.DisplayName
            }
            completed?(result)
        }
    }
    
    func getCountries(completed: (() -> Void)?) {
        
        let ref = Database.database().reference()
        ref.child("Countries").observeSingleEvent(of: .value) { (snapshot) in
            
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                
                if let result = rest.value as? [String: Any] {
                    
                    let country = FSerializer.deserialize(FCountry.self, result, rest.key)!
                    self.Countries[rest.key] = country
                    
                    // sync cache
                    
                    let query2 = ref.child("Countries/\(country.Id)/Cache/Notifications")
                    query2.observe(.childChanged) { (snapshot) in
                        Utils.appDelegate().becomeView?.become([FCache.self], nil)
                    }
                    
                    query2.observe(.childAdded) { (snapshot) in
                        Utils.appDelegate().becomeView?.become([FCache.self], nil)
                    }
                    
                    query2.observe(.childRemoved) { (snapshot) in
                        Utils.appDelegate().becomeView?.become([FCache.self], nil)
                    }
                }
            }
            
            completed?()
        }
    }
    
    func getMe(_ country: String? = nil, completed: ((FProfile) -> Void)?) {
        
        let myPhone = self.myPhone!
        let ref = Database.database().reference()
        let query = ref.child("Profiles").queryOrdered(byChild: "Phone").queryEqual(toValue: myPhone)
        
        var returnComplete = true
        query.observe(.value, with: { [unowned self] (snapshot) in
            
            if let r = snapshot.value as? [String: Any] {
                
                if let result = r[myPhone] as? [String: Any] {
                    
                    let profile = FSerializer.deserialize(FProfile.self, result, myPhone)!
                    self.profiles[myPhone] = profile
                    
                    if returnComplete {
                        returnComplete = false
                        completed?(self.me)
                    }
                    else {
                        Utils.appDelegate().becomeView?.become([FProfile.self], nil)
                    }
                }
            }
            else if !self.removingProfile {
                // create a new profile
                let profile = FSerializer.deserialize(FProfile.self, [
                        "Phone": myPhone
                        ,"Country": country ?? "Belarus"
                        ,"Device": [
                            "Model": "Iphone"
                            ,"Udid": Utils.appDelegate().udid
                        ]
                    ], myPhone)!
                
                profile.save(completed: nil)
            }
        })
    }
    
    func getClubs(completed: (() -> Void)?) {
        let ref = Database.database().reference()
        let query = ref.child("Clubs")
        
        query.observeSingleEvent(of: .value) { (snapshot) in
            
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                if let result = rest.value as? [String: Any] {
                    
//                    print(result)
                    let club = FSerializer.deserialize(FClub.self, result, rest.key)!
                    club.Gallery.forEach({
                        $0.value.Id = $0.key
                    })
                    club.Feed.Entries.forEach({
                        $0.value.Id = $0.key
                    })
                    
                    self.clubs[rest.key] = club
                }
            }
            
            // clean the cache
            var list = self.me.Cache.Subscribers[self.me.Phone] ?? [String]()
            let set1 = Set(self.clubs.map({ $0.key }))
            let set2 = Set(list)
            let set3 = set2.subtracting(set1)
            list.removeAll(where: { set3.contains($0) })
            self.me.Cache.Subscribers[self.me.Phone] = list
            self.me.saveCach(completed: nil)
            
            completed?()
        }
        
        query.observe(.childAdded) { (snapshot) in
            
            if let result = snapshot.value as? [String: Any] {
                
                print("club added \(snapshot.key)")
                let club = FSerializer.deserialize(FClub.self, result, snapshot.key)!
                club.Gallery.forEach({
                    $0.value.Id = $0.key
                })
                club.Feed.Entries.forEach({
                    $0.value.Id = $0.key
                })
                
                self.clubs[snapshot.key] = club
                Utils.appDelegate().becomeView?.become([FClub.self], nil)
            }
        }
        
        query.observe(.childChanged) { (snapshot) in
            
            if let result = snapshot.value as? [String: Any] {
                
                print("club changed \(snapshot.key)")
                let club = FSerializer.deserialize(FClub.self, result, snapshot.key)!
                club.Gallery.forEach({
                    $0.value.Id = $0.key
                })
                club.Feed.Entries.forEach({
                    $0.value.Id = $0.key
                })
                
                self.clubs[snapshot.key] = club
                Utils.appDelegate().becomeView?.become([FClub.self], nil)
            }
        }
        
        query.observe(.childRemoved) { (snapshot) in
            
            print("club removed \(snapshot.key)")
            self.clubs.removeValue(forKey: snapshot.key)
            self.me.Cache.Subscribers.removeValue(forKey: snapshot.key)
            self.me.saveCach(completed: nil)
            Utils.appDelegate().becomeView?.become([FClub.self], nil)
        }
    }
    
    func createProfile(_ phone: String, _ firstName: String, _ lastName: String, _ skills: [String] = [], completed: ((_ profile: FProfile?) -> Void)?) {
        
        let me = FManager.manager().me!
        let profile = FSerializer.deserialize(FProfile.self, [
            "Phone": phone
            ,"FirstName": firstName
            ,"LastName": lastName
            ,"Country": me.Country
            ,"Skills": skills
            ], phone)!
        
        profile.save(completed: { (error) in
            
            if let _ = error {
                completed?(nil)
                return
            }
            
            completed?(profile)
        })
    }
    
    func deleteAccount(completed: (() -> Void)?) {
        
        guard let me = self.me else {
            completed?()
            return
        }
        
        self.removingProfile = true
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference()
        
        let oldImageRef = storageRef.child("Images").child("Profiles").child(me.Phone)
        let oldVideoRef = storageRef.child("Videos").child("Profiles").child(me.Phone)
        
        ref.child("Profiles").queryOrdered(byChild: "Phone").removeAllObservers()
        
        // delete the storage
        oldImageRef.delete(completion: nil)
        oldVideoRef.delete(completion: nil)
        
        // remove profile from all clubs
        let iClubs = me.Cache.Subscribers[me.Phone] ?? [String]()
        let clubs = self.clubs.filter({ iClubs.contains($0.key) }).map({ $0.value })
        clubs.forEach { (club) in
            
            club.Subscribers.removeValue(forKey: me.Phone)
            club.Selected.removeValue(forKey: me.Phone)
            
            ref.child("Clubs/\(club.Id)").updateChildValues(club.serialize())
        }
        
        // delete clubs where profile is an owner
        let myClubs = self.clubs.filter({ $0.value.Owner == me.Phone }).map({ $0.value })
        myClubs.forEach({
            me.deleteClub($0, completed: nil)
        })
        
        // delete profile
        let oldProfileRef = ref.child("Profiles").child(me.Phone)
        oldProfileRef.removeValue { (error, dbref) in
            
            self.Countries.removeAll()
            self.profiles.removeAll()
            
            completed?()
        }
    }
}

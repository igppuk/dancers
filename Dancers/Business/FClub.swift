//
//  FClub.swift
//  Dancers
//
//  Created by MacOS on 03.06.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import Foundation
import UIKit
import FirebaseUI
import FirebaseStorage

enum EStatus: Int, Codable {
    
    // common statuses
    
    case unknown = 0
    
    // club's statuses
    
    case waitingForApproval = 1
    case approved = 2
    case rejected = 3
    
    // profile's statuses
    
    case subscriber = 4
    case admin = 5
    case moderator = 6
    
    func toString() -> String {
        switch self {
        case .unknown:
            return "UNKNOWN_STATUS_STRING".localized
        case .waitingForApproval:
            return "WAITING_STATUS_STRING".localized
        case .approved:
            return "APPROVE_STATUS_STRING".localized
        case .rejected:
            return "REJECTED_STATUS_STRING".localized
        case .subscriber:
            return "SUBSCRIBER_STATUS_STRING".localized
        case .admin:
            return "ADMIN_STATUS_STRING".localized
        case .moderator:
            return "MODERATOR_STATUS_STRING".localized
        }
    }
}

class FStatus: FProtocol, Codable {
    
    var Id = ""
    var Status: EStatus = .unknown
    var Solver = ""
    var Reason = ""
    var Created = Date()
    
    enum CodingKeys: String, CodingKey {
        
        case Status
        case Solver
        case Reason
        case Created
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Status) {
            self.Status = try container.decode(EStatus.self, forKey: .Status)
        }
        if container.contains(.Solver) {
            self.Solver = try container.decode(String.self, forKey: .Solver)
        }
        if container.contains(.Reason) {
            self.Reason = try container.decode(String.self, forKey: .Reason)
        }
        if container.contains(.Created) {
            if let dateString = try? container.decode(String.self, forKey: .Created) {
                self.Created = try dateString.decodeDate(container, forKey: .Created)
            }
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
}

class FClub: FProtocol, Codable {
    
    var Id = ""
    var Name = ""
    var Image: FImage! = nil
    var Owner = ""
    var City = ""
    var Address = ""
    var Phone1 = ""
    var Phone2 = ""
    var Phone3 = ""
    var WebSite = ""
    var Description = ""
    var Gallery = [String: FImage]()
    var Status: FStatus! = nil
    var Subscribers = [String: FStatus]()
    var Feed: FFeed! = nil
    var Selected = [String: Bool]()
    var Created = Date()
    
    enum CodingKeys: String, CodingKey {
        
        case Name
        case Image
        case Owner
        case City
        case Address
        case Phone1
        case Phone2
        case Phone3
        case WebSite
        case Description
        case Gallery
        case Status
        case Subscribers
        case Feed
        case Selected
        case Created
    }
    
    required init(from decoder: Decoder) throws {
        
        self.Image = FSerializer.deserialize(FImage.self, [:])
        self.Status = FSerializer.deserialize(FStatus.self, [:])
        self.Feed = FSerializer.deserialize(FFeed.self, [:])
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Name) {
            self.Name = try container.decode(String.self, forKey: .Name)
        }
        if container.contains(.Image) {
            self.Image = try container.decode(FImage.self, forKey: .Image)
        }
        if container.contains(.Owner) {
            self.Owner = try container.decode(String.self, forKey: .Owner)
        }
        if container.contains(.City) {
            self.City = try container.decode(String.self, forKey: .City)
        }
        if container.contains(.Address) {
            self.Address = try container.decode(String.self, forKey: .Address)
        }
        if container.contains(.Phone1) {
            self.Phone1 = try container.decode(String.self, forKey: .Phone1)
        }
        if container.contains(.Phone2) {
            self.Phone2 = try container.decode(String.self, forKey: .Phone2)
        }
        if container.contains(.Phone3) {
            self.Phone3 = try container.decode(String.self, forKey: .Phone3)
        }
        if container.contains(.WebSite) {
            self.WebSite = try container.decode(String.self, forKey: .WebSite)
        }
        if container.contains(.Description) {
            self.Description = try container.decode(String.self, forKey: .Description)
        }
        if container.contains(.Gallery) {
            self.Gallery = try container.decode([String: FImage].self, forKey: .Gallery)
        }
        if container.contains(.Status) {
            self.Status = try container.decode(FStatus.self, forKey: .Status)
        }
        if container.contains(.Subscribers) {
            self.Subscribers = try container.decode([String: FStatus].self, forKey: .Subscribers)
        }
        if container.contains(.Feed) {
            self.Feed = try container.decode(FFeed.self, forKey: .Feed)
        }
        if container.contains(.Selected) {
            self.Selected = try container.decode([String: Bool].self, forKey: .Selected)
        }
        if container.contains(.Created) {
            if let dateString = try? container.decode(String.self, forKey: .Created) {
                self.Created = try dateString.decodeDate(container, forKey: .Created)
            }
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
    
    // MARK: Methods
    
    func deleteGalleryImage(_ fimage: FImage, completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference()
        let oldImageRef = storageRef.child("Clubs/\(self.Id)/Gallery/\(fimage.Name)")
        
        // delete the storage
        oldImageRef.delete { (error) in
            
            if let error = error {
                completed?(error)
            }
            else {
                self.Gallery.removeValue(forKey: fimage.Id)
                ref.child("Clubs/\(self.Id)").updateChildValues(self.serialize()) { (error, dbref) in
                    
                    // update club
                    completed?(error)
                }
            }
        }
    }
    
    func addGalleryImage(_ fimage: FImage, completed: (() -> Void)?) {
        
        FManager.manager().asyncManager.addObject(fimage.uploadOperation, nil)
        FManager.manager().asyncManager.completedHanlder = { (operation) in
            print("async operation \(operation.body.Name) \(operation.body.UniqueId) completed")
            
            // update club's gallery
            self.Gallery[fimage.Id] = fimage
            self.save { (error) in
                completed?()
            }
        }
   }
    
    func saveEntry(_ entry: FFeedEntry, progressHandler: ((Float) -> Void)?, completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        self.Feed.Entries[entry.Id] = entry
        
        // async upload images
        var p = [String: Float]()
        let images = entry.Text.filter({ $0.Image.Image != nil }).map({ $0.Image! })
        images.forEach({ (fimage) in
            
            if fimage.Image != nil {
                fimage.Id = FManager.newKey()
                fimage.uploadOperation = FeedImageUploadOperation()
                fimage.uploadOperation.getImage = { return fimage }
                fimage.uploadOperation.getClub = { [unowned self] in return self }
                p[fimage.uploadOperation.UniqueId] = 0.0
                FManager.manager().asyncManager.addObject(fimage.uploadOperation, nil)
            }
        })
        
        if images.count > 0 {
            
            var done = p.count
            FManager.manager().asyncManager.completedHanlder = { (operation) in
                
                done -= 1
                print("async operation \(operation.body.Name) \(operation.body.UniqueId) completed, done \(done)")
                if done <= 0 {
                    
                    // update club's feed
                    ref.child("Clubs/\(self.Id)/Feed/").updateChildValues(self.Feed.serialize()) { (error, dbref) in
                        completed?(error)
                    }
                }
            }
            
            FManager.manager().asyncManager.progressHandler = { (operation, progress) in
                print("async operation \(operation.body.Name) \(operation.body.UniqueId) progress \(progress)")
                
                p[operation.body.UniqueId] = progress
                let current = p.values.reduce(0.0) { (result, acc) -> Float in result + acc } / Float(images.count)
                progressHandler?(current)
            }
        }
        else {
            progressHandler?(100.0)
            // update club's feed
            ref.child("Clubs/\(self.Id)/Feed/").updateChildValues(self.Feed.serialize()) { (error, dbref) in
                completed?(error)
            }
        }
    }
    
    func deleteEntry(_ entry: FFeedEntry, completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference()
        
        self.Feed.Entries.removeValue(forKey: entry.Id)
        
        // remove images
        entry.Text.forEach { (part) in
            if !part.Image.IsEmpty {
                let imageRef = storageRef.child("Clubs/\(self.Id)/Feed/\(part.Image.Name)")
                imageRef.delete(completion: nil)
            }
        }
        
        ref.child("Clubs/\(self.Id)/Feed/").updateChildValues(self.Feed.serialize()) { (error, dbref) in
            completed?(error)
        }
    }
    
    func save(completed: ((Error?) -> Void)?) {
        let ref = Database.database().reference()
        ref.child("Clubs/\(self.Id)").updateChildValues(self.serialize()) { (error, dbref) in
            completed?(error)
        }
    }
}

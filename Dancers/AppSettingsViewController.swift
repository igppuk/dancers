//
//  AppSettingsViewController.swift
//  Dancers
//
//  Created by MacOS on 30.05.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit

class ClubTableViewCell: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var StatusLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var EditButton: UIButton!
    @IBOutlet weak var JoinButton: UIButton!
    @IBOutlet weak var ExitButton: UIButton!
    @IBOutlet weak var AddPostButton: UIButton!
    
    // MARK: Closures
    
    var joinHandler: ((_ club: FClub) -> Void)?
    var exitHandler: ((_ club: FClub) -> Void)?
    var editHandler: ((_ club: FClub) -> Void)?
    var addPostHandler: ((_ club: FClub) -> Void)?
    
    // MARK: Properties
    
    weak var Club: FClub!
    
    // MARK: Methods
    
    func show() {
        
        let me = FManager.manager().me!
        
        self.EditButton.isHidden = true
        self.JoinButton.isHidden = true
        self.ExitButton.isHidden = true
        self.AddPostButton.isHidden = true
        
        if self.Club.Owner == me.Phone {
            self.EditButton.isHidden = false
        }
        
        if self.Club.Status.Status == .approved {
            
            self.EditButton.isHidden = false
            if self.Club.Subscribers.contains(where: { $0.key == me.Phone }) {
                self.ExitButton.isHidden = !(self.Club.Owner != me.Phone)
            }
            else {
                self.JoinButton.isHidden = !(self.Club.Owner != me.Phone)
            }
            
            self.AddPostButton.isHidden = !(self.Club.Owner == me.Phone)
        }
        
        self.NameLabel.text = self.Club?.Name
        self.StatusLabel.text = self.Club?.Status.Status.toString()
        self.DescriptionLabel.text = self.Club?.Description
        
        Utils.setImageFromCachOrDownload(self.Club?.Image.Url, imageView: self.ImageView, placeholderImage: UIImage(named: "questionmark"), completedHandler: nil)
    }
    
    class func optimalHeight(_ club: FClub, _ size: CGSize) -> CGFloat {
        let descriptionHeight = club.Description.heightWithConstrainedWidth(width: size.width, font: UIFont(name: "Roboto", size: 17.0)!)
        
        return 70.0 + descriptionHeight + 45.0
    }
    
    // MARK: Actions
    
    @IBAction func joinClicked(_ sender: Any) {
        self.joinHandler?(self.Club)
    }
    
    @IBAction func exitClicked(_ sender: Any) {
        self.exitHandler?(self.Club)
    }
    
    @IBAction func editClicked(_ sender: Any) {
        self.editHandler?(self.Club)
    }
    
    @IBAction func addPostClicked(_ sender: Any) {
        self.addPostHandler?(self.Club)
    }
}

class AppSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, BecomeAppProtocol {
    
    // MARK: Outlets
    
    @IBOutlet weak var ProfileImageView: UIImageView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var AddClubView: UIView!
    @IBOutlet weak var ClubsTableView: UITableView!
    
    // MARK: Properties
    
    var data = [FClub]()
    
    // MARK: Closures

    // MARK: UIViewController methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.AddClubView.makeShadow()
        refresh()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Utils.appDelegate().becomeView = self
    }
    
    // MARK: Methods
    
    func refresh() {
        
        let me = FManager.manager().me!
        
        self.NameLabel.text = me.DisplayName
        Utils.setImageFromCachOrDownload(me.Image.Url, imageView: self.ProfileImageView, completedHandler: nil)
        
        var clubs = FManager.manager().clubs.map({ $0.value })
            .filter({ $0.Owner == me.Phone || $0.Status.Status == .approved })
        clubs = clubs.sorted(by: { $0.Name < $1.Name })
        self.data.removeAll()
        self.data.append(contentsOf: clubs)
        self.ClubsTableView.reloadData()
    }
    
    // MARK: BecomeAppProtocol
    
    func become(_ changes: [FProtocol.Type], _ value: Any?) {
        if changes.contains(where: { $0 == FProfile.self || $0 == FClub.self }) {
            self.refresh()
        }
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let club = self.data[indexPath.row]
        var size = tableView.frame.size
        return ClubTableViewCell.optimalHeight(club, size)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "clubTableViewCell", for: indexPath) as? ClubTableViewCell else {
            fatalError("Wrong cell")
        }
        
        let club = self.data[indexPath.row]
        cell.Club = club
        cell.show()
        
        let me = FManager.manager().me!
        cell.editHandler = nil
        cell.editHandler = { (c) in
            
            let clubController = self.storyboard!.instantiateViewController(withIdentifier: "clubDetailsViewController") as! ClubDetailsViewController
            
            clubController.modalPresentationStyle = .fullScreen
            clubController.club = c
            clubController.closeHandler = {
                
                clubController.dismiss(animated: true) {
                    self.refresh()
                }
            }
            
            self.present(clubController, animated: true, completion: nil)
        }
        
        cell.joinHandler = nil
        cell.joinHandler = { (c) in
            Utils.showConfirm("JOIN_CLUB_STRING".localized, above: self) { (yes) in
                
                Utils.showProgress(self)
                me.joinToClub(c) { (error) in
                    Utils.hideProgress(self)
                    Utils.showErrorIfNeeded(error)
                }
            }
        }
        
        cell.exitHandler = nil
        cell.exitHandler = { (c) in
            Utils.showConfirm("EXIT_CLUB_STRING".localized, above: self) { (yes) in
                
                Utils.showProgress(self)
                me.exitClub(c) { (error) in
                    Utils.hideProgress(self)
                    Utils.showErrorIfNeeded(error)
                }
            }
        }
        
        cell.addPostHandler = nil
        cell.addPostHandler = { (c) in
            let postController = self.storyboard!.instantiateViewController(withIdentifier: "newPostViewController") as! NewPostViewController
             
//          clubController.modalPresentationStyle = .fullScreen
                
            postController.Club = c
            postController.closeHandler = {
                postController.dismiss(animated: true) {
                    self.refresh()
                }
            }
            
            postController.addedHandler = {
                postController.dismiss(animated: true) {
                    Utils.appDelegate().activeTab = 0
                }
            }
             
            self.present(postController, animated: true, completion: nil)
        }
        
        return cell
    }
    
    // MARK: Actions
    
    @IBAction func profileClicked(_ gestureRecognizer : UITapGestureRecognizer) {
        
        let profileSettings = self.storyboard!.instantiateViewController(withIdentifier: "profileSettingsViewController") as! ProfileSettingsViewController
        
        profileSettings.closeHandler = {
            
            profileSettings.dismiss(animated: true) { [unowned self] in
                self.refresh()
            }
        }
        
        self.present(profileSettings, animated: true, completion: nil)
    }
    
    @IBAction func addClubClicked(_ sender: Any) {
        Utils.showConfirm("CREATE_GROUP_CONFIRMATION_STRING".localized, above: self) { (yes) in
            if yes {
                
                let clubController = self.storyboard!.instantiateViewController(withIdentifier: "clubDetailsViewController") as! ClubDetailsViewController
                
                clubController.modalPresentationStyle = .fullScreen
                clubController.closeHandler = {
                    
                    clubController.dismiss(animated: true) { [unowned self] in
                        self.refresh()
                    }
                }
                
                self.present(clubController, animated: true, completion: nil)
            }
        }
    }
}

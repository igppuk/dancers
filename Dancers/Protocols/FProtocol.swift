//
//  FProtocol.swift
//  Pazitive
//
//  Created by Владимир on 28.01.2019.
//  Copyright © 2019 ciferton. All rights reserved.
//

import Foundation

@objc protocol FProtocol {
    
    // MARK: Propert
    
    var Id: String { get set }
    
    // MARK: Methods
    
    @objc optional func syncChildrens(_ id: String)
    func serialize() -> [String: Any]
}

//class FMinProtocol: FProtocol, Codable {
//    
//    var Id = ""
//    
//    required init(from decoder: Decoder) throws {
//    }
//    
//    func serialize() -> [String: Any] {
//        do {
//            let encode = try FSerializer.serialize(self)
//            return encode
//        }
//        catch {
//            print(error)
//        }
//        
//        return [:]
//    }
//}

//
//  GroupViewCell.swift
//  Dancers
//
//  Created by MacOS on 16.07.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit

class ClubViewCell: UIView {
    
    // MARK: Outlets
    
    @IBOutlet weak var ClubView: UIView!
    @IBOutlet weak var ClubImageView: UIImageView!
    @IBOutlet weak var ProfileImageView: UIImageView!
    @IBOutlet weak var ClubNameLabel: UILabel!
    @IBOutlet weak var ClubCityLabel: UILabel!
    @IBOutlet weak var PostsLabel: UILabel!
    @IBOutlet weak var ClubStreetLabel: UILabel!
    
    // MARK: Properties
    
    var Club: FClub!
    
    // MARK: Closures
    
    // MARK: Methods
    
    func show() {
        
        var cPosts = 0
        
        self.ClubCityLabel.text = nil
        self.ClubStreetLabel.text = nil
        ClubView.dropShadow()
        
        if self.Club != nil {
         
            self.ClubNameLabel.text = self.Club.Name
            self.ClubCityLabel.text = self.Club.City
            self.ClubStreetLabel.text = self.Club.Address
            
            Utils.setImageFromCachOrDownload(self.Club.Image.Url, imageView: self.ClubImageView, completedHandler: nil)
            
            FManager.manager().getProfiles([self.Club.Owner]) { (profiles) in
                if let owner = profiles.first {
                    Utils.setImageFromCachOrDownload(owner.Image.Url, imageView: self.ProfileImageView, completedHandler: nil)
                }
            }
            
            cPosts = self.Club.Feed.Entries.map({ $0.value }).count
        }
        
        let postsText = "POSTS_COUNT_STRING".localizedFormat("\(cPosts)")
        let postsWidth = postsText.widthOfString(usingFont: self.PostsLabel.font) + 20.0
        self.PostsLabel.frame.size.width = postsWidth
        self.PostsLabel.text = postsText
    }
}

//
//  XIBLocalizable.swift
//  Pocket.CEO
//
//  Created by MacOS on 07.05.2020.
//  Copyright © 2020 Vladimir Lozhnikov. All rights reserved.
//

import UIKit

protocol Localizable {
    var localized: String { get }
}

extension String: Localizable {
    var localized: String {
        
        /*!!!let langData = FManager.manager().langData
        if let loc = langData.first(where: { $0.Id == self }) {
            return loc.Text
        }*/
        
        return NSLocalizedString(self, comment: "")
    }
    
    func localizedFormat(_ arguments: CVarArg...) -> String {
        return String(format: self.localized, arguments: arguments)
    }
}

protocol XIBLocalizable {
    var xibLocKey: String? { get set }
}

extension UILabel: XIBLocalizable {
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
//            let r = key?.localized
//            print(key)
//            print(r)
            text = key?.localized
        }
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
   }
}

extension UITextField: XIBLocalizable {
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
            text = key?.localized
        }
    }

    @IBInspectable var xibPlaceholder: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localized
        }
    }
}

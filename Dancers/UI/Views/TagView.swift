//
//  TagView.swift
//  Dancers
//
//  Created by MacOS on 30.05.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit

class TagView: UIView {
    
    // MARK: Outlets
    
    @IBOutlet weak var NameLabel: UILabel!
    
    // MARK: Properties
    
    var Name = ""
    
    var selectedColor = UIColor(red: 0x40/255.0, green: 0x6B/255.0, blue: 0x9E/255.0, alpha: 1)
    var unselectedColor = UIColor(red: 0x40/255.0, green: 0x6B/255.0, blue: 0x9E/255.0, alpha: 0.3)
    
    private var _selected = false
    var isSelected: Bool {
        
        get {
            
            return _selected
        }
        set {
            
            _selected = newValue
            if newValue {
                self.backgroundColor = self.selectedColor
            }
            else {
                self.backgroundColor = self.unselectedColor
            }
        }
    }
    
    var optimizedBounds: CGRect {
        
        get {
            
            let nameWidth = self.Name.widthOfString(usingFont: self.NameLabel.font)
            let rect = CGRect(x: 0.0, y: 0.0, width: nameWidth + 16.0*2.0, height: 40.0)
            
            return rect
        }
    }
    
    // MARK: Methods
    
    func show() {
        
        self.NameLabel.text = self.Name
        
        if self.isSelected {
            self.backgroundColor = self.selectedColor
        }
        else {
            self.backgroundColor = self.unselectedColor
        }
    }
}

//
//  Extensions.swift
//  CEODailyNew
//
//  Created by MacBook Pro on 25.05.16.
//  Copyright © 2016 ciferton. All rights reserved.
//

import Foundation
import UIKit
import PhoneNumberKit

extension UIColor {
    
    class func fromInt(intValue: Int) -> UIColor {
        
        // &  binary AND operator to zero out other color values
        // >>  bitwise right shift operator
        // Divide by 0xFF because UIColor takes CGFloats between 0.0 and 1.0
        
        let alpha = CGFloat((intValue & 0xFF000000) >> 24) / 0xFF
        let red =   CGFloat((intValue & 0x00FF0000) >> 16) / 0xFF
        let green = CGFloat((intValue & 0x0000FF00) >> 8) / 0xFF
        let blue =  CGFloat(intValue & 0x000000FF) / 0xFF
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    func toInt() -> Int? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
            return rgb
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}

public extension String {
    
    var nilIfEmpty: String? {
        return self.isEmpty ? nil : self
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = self.index(startIndex, offsetBy: r.lowerBound)
        let end = self.index(start, offsetBy: r.upperBound - r.lowerBound)
        return String(self[start ..< end])
    }
    
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F910...0x1F918, // New Emoticons
            0x1F1E6...0x1F1FF, // Flags
            0x1F980...0x1F984,
            0x1F191...0x1F19A,
            0x1F201...0x1F202,
            0x1F232...0x1F23A,
            0x1F250...0x1F251,
            0x23E9...0x23F3,
            0x23F8...0x23FA,
            0x1F170...0x1F171,
            0x1F17E,
            0xA9,
            0xAE,
            0x2122,
            0x2328,
            0x3030,
            0x1F0CF,
            0x1F18E,
            0x1F9C0:
                return true
            default:
                continue
            }
        }
        return false
    }
    
    func fromHtml() -> NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            let attrString = try NSMutableAttributedString(data: data, options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            
            if let lastCharacter = attrString.string.last, lastCharacter == "\n" {
                attrString.deleteCharacters(in: NSMakeRange(attrString.length - 1, 1))
            }
            
            return attrString
        }catch{
            return NSAttributedString()
        }
    }
    
    func decodeDate<K>(_ container: KeyedDecodingContainer<K>, forKey key: K) throws -> Foundation.Date! where K : CodingKey {
        
        if self.isEmpty {
            return nil
        }
        
        let formatter = DateFormatter.firebaseDateFormatter
        if let date = formatter.date(from: self) {
            return date
        } else {
            throw DecodingError.dataCorruptedError(forKey: key, in: container, debugDescription: "Date string does not match format expected by formatter.")
        }
    }
    
    func format(from string: String) -> Date? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = string
        formatter.calendar = Calendar(identifier: .iso8601)
        
        return formatter.date(from: self)
    }
    
    func replace(_ target: String, _ withString: String) -> String
    {
        let newString = NSMutableString(string: self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil))
        return newString as String
    }
    
    mutating func insert(_ string: String, _ ind: Int) {
        self.insert(contentsOf: string, at: self.index(self.startIndex, offsetBy: ind))
    }
}

public extension NSAttributedString {
    
    func toHtml() -> String {
        let attributes = [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.html]
        do {
            let htmlData = try data(from: NSMakeRange(0, self.length), documentAttributes: attributes)
            if let htmlString = String(data: htmlData, encoding: String.Encoding.utf8) {
                return htmlString
            }
        }
        catch {
            print("error creating HTML from Attributed String")
        }
        return String()
    }
}

public extension Int32 {
    
    static func random(_ lower: Int32 = min, upper: Int32 = max) -> Int32 {
        let r = arc4random_uniform(UInt32(Int64(upper) - Int64(lower)))
        return Int32(Int64(r) + Int64(lower))
    }
}

extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let boundingBox = self.size(withAttributes: [NSAttributedString.Key.font: font])
        return boundingBox.width
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
}

extension NSAttributedString {
    
    func widthWithConstrainedHeight(_ height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        var w = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).width
        
        if self.string.containsEmoji {
            w *= 1.3
        }
        
        return ceil(w)
    }
    
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)

        return boundingBox.height
    }
}

extension Date {
    struct InternalDate {
        static let formatterCustom: DateFormatter = {
            let formatter = DateFormatter()
            return formatter
        }()
        static let formatterDate:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return formatter
        }()
        static let formatterTime:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            return formatter
        }()
        static let formatterWeekday:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE"
            return formatter
        }()
        static let formatterMonth:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "LLLL"
            return formatter
        }()
    }
    var date: String {
        return InternalDate.formatterDate.string(from: self)
    }
    var time: String {
        return InternalDate.formatterTime.string(from: self)
    }
    var weekdayName: String {
        return InternalDate.formatterWeekday.string(from: self)
    }
    var monthName: String {
        return InternalDate.formatterMonth.string(from: self)
    }
    func formatted(_ format:String) -> String {
        InternalDate.formatterCustom.dateFormat = format
        return InternalDate.formatterCustom.string(from: self)
    }
    
    static func toString(_ object: Foundation.Date?) -> String {
        if (object != nil) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return dateFormatter.string(from: object!)
        }
        
        return "unknown"
    }
    
    func chatFormatted() -> String {
        
        let now = Date()
        let interval = now.timeIntervalSince(self) // in seconds
        
        switch interval {
        case let x where x < 60: // less then 1 minute
            return "Now"
        case let x where x < 60*60: // less then 1 hour
            return "\(Int(x/60)) Minute(s) ago"
        case let x where x < 60*60*24: // less then 1 day
            return "\(Int(x/(60*60))) Hour(s) ago"
        default: // more then 1 day
            return Date.toString(self)
        }
    }
    
    func jobFormatted() -> String {
        
        let now = Date()
        let interval = now.timeIntervalSince(self) // in seconds
        let dateStr = formatted("dd MMM yyyy")
        
        switch interval {
        case let x where x < 60: // less then 1 minute
            return "TIME_NOW_TEXT".localizedFormat("\(dateStr)") // "\(dateStr), Now"
        case let x where x < 60*60: // less then 1 hour
            let minuteStr = "TIME_MINUTES_AGO_TEXT".localizedFormat("\(Int(x/60))") // "\(Int(x/60)) Minute(s) ago"
            return "\(dateStr), \(minuteStr)"
        case let x where x < 60*60*24: // less then 1 day
            let hoursStr = "TIME_HOURS_AGO_TEXT".localizedFormat("\(Int(x/(60*60)))") // "\(Int(x/(60*60))) Hour(s) ago"
            return "\(dateStr), \(hoursStr)"
        default: // more then 1 day
            let daysStr = "TIME_DAYS_AGO_TEXT".localizedFormat("\(Int(interval/(60*60*24)))") // "\(Int(interval/(60*60*24))) Day(s) ago"
            return "\(dateStr), \(daysStr)"
        }
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var nextDay: Date {
        var components = DateComponents()
        components.day = 1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var startOfWeek: Date! {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        return gregorian.date(byAdding: .day, value: 1, to: sunday)!
    }
    
    var endOfWeek: Date! {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        return gregorian.date(byAdding: .day, value: 7, to: sunday)!
    }
    
    var startOfMonth: Date {
        let components = Calendar.current.dateComponents([.year, .month], from: startOfDay)
        return Calendar.current.date(from: components)!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
}

extension UIView {
    var height: CGFloat {
        get {
            return self.bounds.height
        }
    }
    
    func rotate(angle: CGFloat, animate: Bool = false) {
        
        let radians = angle / 180.0 * CGFloat(Float.pi)
        
        let rotation = self.transform.rotated(by: radians);
        if !animate {
            self.transform = rotation
        }
        else {
            UIView.animate(withDuration: 0.3, animations: {
                self.transform = rotation
            })
        }
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    public class func fromNib(_ nibNameOrNil: String) -> Self {
        return fromNib(nibNameOrNil, type: self)
    }
    
    public class func fromNib<T : UIView>(_ nibNameOrNil: String, type: T.Type) -> T {
        let v: T? = fromNib(nibNameOrNil, type: T.self)
        return v!
    }
    
    public class func fromNib<T : UIView>(_ nibNameOrNil: String, type: T.Type) -> T? {
        var view: T?
        let nibViews = Bundle.main.loadNibNamed(nibNameOrNil, owner: nil, options: nil)
        for v in nibViews! {
            if let tog = v as? T {
                view = tog
            }
        }
        return view
    }
    
    public class var nibName: String {
        let name = "\(self)".components(separatedBy: ".").first ?? ""
        return name
    }
    public class var nib: UINib? {
        if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
            return UINib(nibName: nibName, bundle: nil)
        } else {
            return nil
        }
    }
    
    func dropShadow() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15).cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 4.0
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
    }
    
    func makeShadow() {
        
        self.layer.cornerRadius = 10.0
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15).cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 4.0
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func showError(errorColor: UIColor = UIColor(red: 0.9, green: 0.22, blue: 0.27, alpha: 1)) {
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = errorColor.cgColor
    }
    
    func showError(_ scrollView: UIScrollView, errorColor: UIColor = UIColor(red: 0.9, green: 0.22, blue: 0.27, alpha: 1)) {
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = errorColor.cgColor
        
        let y = self.frame.minY - 50.0
        scrollView.setContentOffset(CGPoint(x: 0.0, y: (y < 0.0) ? 0.0 : y), animated: true)
    }
    
    func clearError(okColor: UIColor = UIColor(red: 0.18, green: 0.18, blue: 0.18, alpha: 0.2)) {
        
        self.superview?.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    func toBack() {
        
        self.layer.borderWidth = 3.0
        self.layer.cornerRadius = 10.0
        self.layer.borderColor = UIColor(red: 105.0/255.0, green: 105.0/255.0, blue: 105.0/255.0, alpha: 0.2).cgColor
    }
    
    func toBack2() {
        
        self.layer.borderWidth = 3.0
        self.layer.cornerRadius = 10.0
        self.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.2).cgColor
    }
    
    func toNext() {
        
        self.layer.borderWidth = 3.0
        self.layer.cornerRadius = 10.0
        self.layer.borderColor = UIColor.white.cgColor
    }
    
    func select(_ width: CGFloat = 2.0, _ color: UIColor = UIColor.red) {
        self.borderWidth = width
        self.borderColor = color
    }
    
    func unselect(_ width: CGFloat = 0.0, _ color: UIColor = UIColor.clear) {
        self.borderWidth = width
        self.borderColor = color
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
}

extension UIDevice {
    
    var iPhone: Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }
    
    var modelName: String {
        #if targetEnvironment(simulator)
            let DEVICE_IS_SIMULATOR = true
        #else
            let DEVICE_IS_SIMULATOR = false
        #endif
        
        var machineString : String = ""
        
        if DEVICE_IS_SIMULATOR == true
        {
            
            if let dir = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                machineString = dir
            }
        }
        else {
            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            machineString = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8, value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
        }
        switch machineString {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        default:                                        return machineString
        }
    }
}

extension UIImage {
    
    func crop(rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x *= self.scale
        rect.origin.y *= self.scale
        rect.size.width *= self.scale
        rect.size.height *= self.scale
        
        let imageRef  = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        
        return image
    }
    
    func cropCenter() -> UIImage {
        var width = self.size.width
        if (self.size.height < width) {
            width = self.size.height
        }
        
        let rect = CGRect(x: 0.0, y: self.size.height/2.0 - width/2.0, width: width, height: width)
        let cropImage = self.crop(rect: rect)
        return cropImage
    }
    
    func resize(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / self.size.width
        let heightRatio = targetSize.height / self.size.height
        
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func scale(iby scale: CGFloat) -> UIImage? {
        let size = self.size
        let scaledSize = CGSize(width: size.width * scale, height: size.height * scale)
        return resize(targetSize: scaledSize)
    }
    
    func thumbnail(for size: CGSize) -> UIImage? {
        
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { (context) in
            self.draw(in: CGRect(origin: .zero, size: size))
        }
    }
    
    /// Fix image orientaton to protrait up
    func fixedOrientation() -> UIImage? {
        guard imageOrientation != UIImage.Orientation.up else {
            // This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            // CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil // Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
        case .up, .upMirrored:
            break
        @unknown default:
            break
        }
        
        // Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
    
    func save(in file: String) -> URL? {
        
        if let data = self.jpegData(compressionQuality: 0.8) {
            let filename = getDocumentsDirectory().appendingPathComponent("\(file).jpg")
            try? data.write(to: filename)
            
            return filename
        }
        
        return nil
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}

extension DateFormatter {
    static let firebaseDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        //formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZZZZZ"
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        //formatter.timeZone = TimeZone(secondsFromGMT: 0)
        //formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}

extension UITableView {
    
    var cells: [UITableViewCell] {
        
        return (0..<self.numberOfSections).indices.map { (sectionIndex:Int) -> [UITableViewCell] in
            return (0..<self.numberOfRows(inSection: sectionIndex)).indices.compactMap{ (rowIndex:Int) -> UITableViewCell? in
                return self.cellForRow(at: IndexPath(row: rowIndex, section: sectionIndex))
            }
            }.flatMap{$0}
    }
}

extension UIButton {
    private func imageWithColor(color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        self.setBackgroundImage(imageWithColor(color: color), for: state)
    }
}

extension UITextField {
    
    func validatePhone(with prefix: String = "", errorText: String = "TRY_AGAIN_STRING".localized) -> Bool {
        
        do {
            
            let phoneNumberKit = PhoneNumberKit()
            let phoneText = "\(prefix)\(self.text ?? "")"
            
            try phoneNumberKit.parse(phoneText)
            self.clearError()
            return true
        }
        catch {
            
            self.showError(errorText: errorText)
            return false
        }
    }
    
    func validate(okText: String = "", errorText: String = "TRY_AGAIN_STRING".localized, okColor: UIColor = UIColor(red: 0.18, green: 0.18, blue: 0.18, alpha: 0.2), errorColor: UIColor = UIColor(red: 0.9, green: 0.22, blue: 0.27, alpha: 1)) -> Bool {
        
        if let t = self.text, t.trim().isEmpty {
            
            self.showError(errorText: errorText, errorColor: errorColor)
            return false
        }
        
        self.clearError(okText: okText, okColor: okColor)
        return true
    }
    
    func showError(errorText: String = "TRY_AGAIN_STRING".localized, errorColor: UIColor = UIColor(red: 0.9, green: 0.22, blue: 0.27, alpha: 1)) {
        
        self.superview?.layer.borderWidth = 3.0
        self.superview?.layer.borderColor = errorColor.cgColor
        
        self.placeholder = errorText
        self.placeholderColor = errorColor
    }
    
    func clearError(okText: String = "", okColor: UIColor = UIColor(red: 0.18, green: 0.18, blue: 0.18, alpha: 0.2)) {
        
        self.superview?.layer.borderColor = UIColor.clear.cgColor
        self.placeholderColor = okColor
        self.placeholder = okText
    }
    
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }
}

extension UITextView {
    
    func validate() -> Bool {
        if let t = self.text, t.isEmpty {
            self.layer.borderColor = UIColor.red.cgColor
            return false
        }
        else {
            self.layer.borderColor = UIColor.green.cgColor
        }
        
        return true
    }
    
    func sizeHeightToFitAttributedString(_ minHeigth: CGFloat, _ maxHeight: CGFloat) {
        let oldWidth = self.bounds.width
        let textSize = self.attributedText.size()
        
        var viewSize = CGSize(width: oldWidth, height: textSize.height + self.firstCharacterOrigin.y * 2)
        if viewSize.height < minHeigth {
            viewSize.height = minHeigth
        }
        else if viewSize.height > maxHeight {
            viewSize.height = maxHeight
        }
        
        self.frame.size = viewSize // self.sizeThatFits(viewSize)
    }

    private var firstCharacterOrigin: CGPoint {
        if self.text.lengthOfBytes(using: .utf8) == 0 {
            return .zero
        }
        let range = self.textRange(from: self.position(from: self.beginningOfDocument, offset: 0)!,
                                     to: self.position(from: self.beginningOfDocument, offset: 1)!)
        return self.firstRect(for: range!).origin

    }
}

/// Extend UITextView and implemented UITextViewDelegate to listen for changes
extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor(red: 0.88, green: 0.88, blue: 0.88, alpha: 1)
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
}

extension URL {
    
    var attributes: [FileAttributeKey : Any]? {
        do {
            return try FileManager.default.attributesOfItem(atPath: path)
        } catch let error as NSError {
            print("FileAttribute error: \(error)")
        }
        return nil
    }
    
    var fileSize: UInt64 {
        return attributes?[.size] as? UInt64 ?? UInt64(0)
    }
    
    var fileSizeString: String {
        return ByteCountFormatter.string(fromByteCount: Int64(fileSize), countStyle: .file)
    }
    
    var creationDate: Date? {
        return attributes?[.creationDate] as? Date
    }
}

extension UISwitch {
    
    @IBInspectable var offTint: UIColor {
        get {
            return self.tintColor
        }
        set {
            self.tintColor = newValue
        }
    }
}

extension DispatchQueue {
    
    static func background(delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
        
        DispatchQueue.global(qos: .background).async {
            
            background?()
            if let completion = completion {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
}

extension UITableView {
    
    func reloadData(completion: (() -> Void)?) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }) { (y) in
            completion?()
        }
    }
}

extension CGAffineTransform {
    var scale: CGFloat {
        return sqrt(CGFloat(a * a + c * c))
    }
}

fileprivate var bindedEvents: [UIButton:EventBinder] = [:]

fileprivate class EventBinder {

    let event: UIControl.Event
    let button: UIButton
    let handler: UIButton.EventHandler
    let selector: Selector

    required init(
        _ event: UIControl.Event,
        on button: UIButton,
        withHandler handler: @escaping UIButton.EventHandler
    ) {
        self.event = event
        self.button = button
        self.handler = handler
        self.selector = #selector(performEvent(on:ofType:))
        button.addTarget(self, action: self.selector, for: event)
    }

    deinit {
        button.removeTarget(self, action: selector, for: event)
        if let index = bindedEvents.index(forKey: button) {
            bindedEvents.remove(at: index)
        }
    }
}

private extension EventBinder {

    @objc func performEvent(on sender: UIButton, ofType event: UIControl.Event) {
        handler(sender, event)
    }
}

extension UIButton {

    typealias EventHandler = (UIButton, UIControl.Event) -> Void

    func on(_ event: UIControl.Event, handler: @escaping EventHandler) {
        bindedEvents[self] = EventBinder(event, on: self, withHandler: handler)
    }
}

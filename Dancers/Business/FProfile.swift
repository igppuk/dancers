//
//  FProfile.swift
//  Pazitive
//
//  Created by Владимир on 19.06.2018.
//  Copyright © 2018 ciferton. All rights reserved.
//

import Foundation
import FirebaseUI
import FirebaseStorage

class FProfile : FProtocol, Codable, IComparable {
    
    var Id = ""
    var Phone = ""
    var FirstName = ""
    var LastName = ""
    var Image: FImage! = nil
    var Device: FDevice! = nil
    var Country: String! = nil
    var City: String! = nil
    var Cache: FCache!
    var Superuser: Bool = false
    var Created = Date()
    var AppSettings: FAppSettings! = nil
    
    typealias CompareEntry = FProfile
    
    enum CodingKeys: String, CodingKey {
        
        case Phone
        case FirstName
        case LastName
        case Image
        case Device
        case Country
        case City
        case Superuser
        case Cache
        case Created
        case AppSettings
    }
    
    var DisplayName: String {
        
        get {
            
            if self.FirstName.isEmpty && self.LastName.isEmpty {
                return "UNKNOWN_NAME_STRING".localized
            }
            return "\(self.FirstName) \(self.LastName)"
        }
    }
    
    var IsEmpty: Bool {
        get {
            
            if self.FirstName.isEmpty || self.LastName.isEmpty || self.Image.Url.isEmpty {
                return true
            }
            return false
        }
    }
    
    var RealCountry: FCountry {
        get {
            let countries = FManager.manager().Countries.map({ $0.value })
            if let country = countries.first(where: { $0.Name == self.Country }) {
                return country
            }
            return FSerializer.deserialize(FCountry.self, [:])
        }
        set {
            self.Country = newValue.Name
        }
    }
    
    var MyClubs: [FClub] {
        let clubs = FManager.manager().clubs.filter({ $0.value.Owner == self.Phone }).map({ $0.value })
        return clubs
    }
    
    var Clubs: [FClub] {
        let clubs = FManager.manager().clubs.map({ $0.value })
            .filter { $0.Owner != self.Phone }
            .filter { $0.Subscribers.contains { (s) -> Bool in s.key == self.Phone }
        }
        
        return clubs
    }
    
    required init(from decoder: Decoder) throws {
        
        self.Image = FSerializer.deserialize(FImage.self, [:])
        self.Device = FSerializer.deserialize(FDevice.self, [:])
        self.Cache = FSerializer.deserialize(FCache.self, [:])
        self.AppSettings = FSerializer.deserialize(FAppSettings.self, [:])
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Phone) {
            self.Phone = try container.decode(String.self, forKey: .Phone)
        }
        if container.contains(.FirstName) {
            self.FirstName = try container.decode(String.self, forKey: .FirstName)
        }
        if container.contains(.LastName) {
            self.LastName = try container.decode(String.self, forKey: .LastName)
        }
        if container.contains(.Image) {
            do {
                self.Image = try container.decode(FImage.self, forKey: .Image)
            }
            catch {
                print(error)
            }
        }
        if container.contains(.Device) {
            self.Device = try container.decode(FDevice.self, forKey: .Device)
        }
        if container.contains(.Country) {
            self.Country = try container.decode(String.self, forKey: .Country)
        }
        if container.contains(.City) {
            self.City = try container.decode(String.self, forKey: .City)
        }
        if container.contains(.Cache) {
            self.Cache = try container.decode(FCache.self, forKey: .Cache)
        }
        if container.contains(.Created) {
            if let dateString = try? container.decode(String.self, forKey: .Created) {
                self.Created = try dateString.decodeDate(container, forKey: .Created)
            }
        }
        if container.contains(.Superuser) {
            self.Superuser = try container.decode(Bool.self, forKey: .Superuser)
        }
        if container.contains(.AppSettings) {
            self.AppSettings = try container.decode(FAppSettings.self, forKey: .AppSettings)
        }
    }
    
    // MARK: IComparable
    
    func CompareTo(_ other: FProfile) -> [FProtocol.Type] {
    
        var result: [FProtocol.Type] = []
        
        if self.Phone != other.Phone || self.FirstName != other.FirstName || self.LastName != other.LastName || self.Image.Url != other.Image.Url || self.Country != other.Country || self.City != other.City {
            result.append(FProfile.self)
        }
                
        return result
    }
    
    // MARK: Methods
    
    func save(completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference()
        let imageName = "\(FManager.newKey()).jpg"
        
        let updateHandler = {
            
            // update profile
            ref.child("Profiles").child(self.Phone).updateChildValues(self.serialize(), withCompletionBlock: { (error, dbref) in
                
                completed?(error)
            })
        }
        
        // remove old image
        if let imageData = self.Image?.tempImageData {
            
            let oldImageRef = storageRef.child("Images").child("Profiles").child(self.Phone).child(self.Image.Name)
            oldImageRef.delete { [unowned self] (error) in
                
                self.Image.Name = imageName
                
                // upload new image
                let imageRef = storageRef.child("Images").child("Profiles").child(self.Phone).child(self.Image.Name)
                imageRef.putData(imageData, metadata: nil, completion: { (metadata, error) in
                    
                    imageRef.downloadURL(completion: { (url, error1) in
                        
                        self.Image.Url = url!.absoluteString
                        updateHandler()
                    })
                })
            }
        }
        else {
            // update profile
            updateHandler()
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
    
    func saveClub(_ club: FClub, progressHandler: ((Float) -> Void)?, completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        
        let operationClosure = { (_ images: [FImage]) in
            
            // async upload images
            var p = [String: Float]()
            images.forEach({
                p[$0.uploadOperation.UniqueId] = 0.0
                FManager.manager().asyncManager.addObject($0.uploadOperation, nil)
            })
            
            var done = p.count
            FManager.manager().asyncManager.completedHanlder = { (operation) in
                done -= 1
                print("async operation \(operation.body.Name) \(operation.body.UniqueId) completed, done \(done)")
                if done <= 0 {
                    
                    // update club
                    ref.child("Clubs/\(club.Id)").updateChildValues(club.serialize()) { (error, dbref) in
                        
                        // update profile's cache
                        var list = self.Cache.Subscribers[self.Phone] ?? [String]()
                        if !list.contains(self.Phone) {
                            list.append(club.Id)
                        }
                        self.Cache.Subscribers[self.Phone] = list
                        self.saveCach(completed: nil)
                        completed?(error)
                    }
                }
            }
            
            FManager.manager().asyncManager.progressHandler = { (operation, progress) in
                print("async operation \(operation.body.Name) \(operation.body.UniqueId) progress \(progress)")
                
                p[operation.body.UniqueId] = progress
                let current = p.values.reduce(0.0) { (result, acc) -> Float in result + acc } / Float(1 + club.Gallery.count)
                progressHandler?(current)
            }
        }
        
        if !club.Id.isEmpty {
            
            // remove old image
            if let _ = club.Image?.tempImageData {
                
                // delete old image
                let storageRef = Storage.storage().reference()
                let oldImageRef = storageRef.child("Clubs/\(club.Id)/\(club.Image.Name)")
                
                // delete the storage
                oldImageRef.delete { (error) in
                    operationClosure([club.Image])
                }
            }
            else {
                // update club
                ref.child("Clubs/\(club.Id)").updateChildValues(club.serialize()) { (error, dbref) in
                    completed?(error)
                }
            }
        }
        else {
            // add new club
            club.Id = FManager.newKey()
            var images = club.Gallery.map({ $0.value })
            images.append(club.Image)
            operationClosure(images)
                        
            /*
            // async upload images
            var p = [String: Float]()
            p[club.Image.uploadOperation.UniqueId] = 0.0
            FManager.manager().asyncManager.addObject(club.Image.uploadOperation, nil)
            club.Gallery.forEach({
                p[$0.value.uploadOperation.UniqueId] = 0.0
                FManager.manager().asyncManager.addObject($0.value.uploadOperation, nil)
            })
            
            var done = p.count
            FManager.manager().asyncManager.completedHanlder = { (operation) in
                done -= 1
                print("async operation \(operation.body.Name) \(operation.body.UniqueId) completed, done \(done)")
                if done <= 0 {
                    
                    // update club
                    ref.child("Clubs/\(club.Id)").updateChildValues(club.serialize()) { (error, dbref) in
                        
                        // update profile's cache
                        if self.Phone == club.Owner {
                            var list = self.Cache.Subscribers[self.Phone] ?? [String]()
                            list.append(club.Id)
                            self.Cache.Subscribers[self.Phone] = list
                            self.saveCach(completed: nil)
                        }
                        completed?(error)
                    }
                }
            }
            
            FManager.manager().asyncManager.progressHandler = { (operation, progress) in
                print("async operation \(operation.body.Name) \(operation.body.UniqueId) progress \(progress)")
                
                p[operation.body.UniqueId] = progress
                let current = p.values.reduce(0.0) { (result, acc) -> Float in result + acc } / Float(1 + club.Gallery.count)
                progressHandler?(current)
            }
 */
        }
    }
    
    func deleteClub(_ club: FClub, completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        let storageRef = Storage.storage().reference()
        
        // clear user's cache
        self.Cache.Subscribers[self.Phone]?.removeAll(where: { $0 == club.Id })
        self.saveCach(completed: nil)
        
        // clear storage
        storageRef.child("Clubs/\(club.Id)/\(club.Image.Name)").delete(completion: nil)
        club.Gallery.forEach({
            storageRef.child("Clubs/\(club.Id)/Gallery/\($0.value.Name)").delete(completion: nil)
        })
        
        ref.child("Clubs/\(club.Id)").removeValue { (error, dbref) in
            completed?(error)
        }
    }
    
    func joinToClub(_ club: FClub, completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        
        let status = FSerializer.deserialize(FStatus.self, [
            "Status": EStatus.subscriber.rawValue
        ])
        
        club.Subscribers[self.Phone] = status
        var list = self.Cache.Subscribers[self.Phone] ?? [String]()
        list.append(club.Id)
        self.Cache.Subscribers[self.Phone] = list
        
        ref.child("Profiles/\(self.Phone)").updateChildValues(self.serialize()) { (error, dbref) in
            ref.child("Clubs/\(club.Id)").updateChildValues(club.serialize()) { (error1, dbref1) in
                completed?(error ?? error1)
            }
        }
    }
    
    func exitClub(_ club: FClub, completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        
        club.Subscribers.removeValue(forKey: self.Phone)
        club.Selected.removeValue(forKey: self.Phone)
        var list = self.Cache.Subscribers[self.Phone] ?? [String]()
        list.removeAll(where: { $0 == club.Id })
        self.Cache.Subscribers[self.Phone] = list
        
        ref.child("Profiles/\(self.Phone)").updateChildValues(self.serialize()) { (error, dbref) in
            ref.child("Clubs/\(club.Id)").updateChildValues(club.serialize()) { (error1, dbref1) in
                completed?(error ?? error1)
            }
        }
    }
    
    func saveCach(completed: ((Error?) -> Void)?) {
        
        if let _ = self.Cache, !FManager.manager().removingProfile {
            
            let ref = Database.database().reference()
            
            // update cahce
            ref.child("Profiles").child(self.Phone).child("Cache").updateChildValues(self.Cache.serialize(), withCompletionBlock: { (error, dbref) in
                
                completed?(error)
            })
        }
        else {
            completed?(nil)
        }
    }
}

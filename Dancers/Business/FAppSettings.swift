//
//  FAppSettings.swift
//  Dancers
//
//  Created by MacOS on 01.06.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import Foundation

class FAppSettings: FProtocol, Codable {
    
    var Id = ""
    var Created = Date()
    
    enum CodingKeys: String, CodingKey {
        
        case Created
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Created) {
            if let dateString = try? container.decode(String.self, forKey: .Created) {
                self.Created = try dateString.decodeDate(container, forKey: .Created)
            }
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
}

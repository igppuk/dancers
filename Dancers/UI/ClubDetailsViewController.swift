//
//  ClubDetailsViewController.swift
//  Dancers
//
//  Created by MacOS on 01.06.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit

class GalleryCellView: UICollectionViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var AddButton: UIButton!
    @IBOutlet weak var ImageView: UIImageView!
    
    // MARK: Properties
    
    weak var image: FImage!
    
    // MARK: Closures
    
    var addHandler: (() -> Void)?
    var showHandler: ((_ image: FImage) -> Void)?
    var deleteHandler: ((_ image: FImage) -> Void)?
    
    // MARK: Methods
    
    func show() {
        self.ImageView.image = nil
        Utils.setImageFromCachOrDownload(self.image?.Url, imageView: self.ImageView) { [unowned self] (image) in
            if image == nil {
                self.ImageView.image = self.image?.Image ?? nil
            }
        }
        
        let plusText = (self.image == nil) ? "+" : ""
        self.AddButton.setTitle(plusText, for: .normal)
        self.ImageView.isHidden = self.image == nil
    }
    
    // MARK: override
    
    override func delete(_ sender: Any?) {
        self.deleteHandler?(self.image)
    }
    
    // MARK: Actions
    
    @IBAction func buttonClicked(_ sender: Any) {
        if self.image == nil {
            self.addHandler?()
        }
        else {
            self.showHandler?(self.image)
        }
    }
}

class ClubDetailsViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, BecomeAppProtocol {
    
    // MARK: Outlets
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var ClubImageView: UIImageView!
    @IBOutlet weak var NameView: UIView!
    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var OwnerView: UIView!
    @IBOutlet weak var OwnerTextField: UITextField!
    @IBOutlet weak var CityView: UIView!
    @IBOutlet weak var CityTextField: UITextField!
    @IBOutlet weak var AddressView: UIView!
    @IBOutlet weak var AddressTextView: UITextView!
    @IBOutlet weak var Phone1View: UIView!
    @IBOutlet weak var Phone1TextField: UITextField!
    @IBOutlet weak var Phone2View: UIView!
    @IBOutlet weak var Phone2TextField: UITextField!
    @IBOutlet weak var Phone3View: UIView!
    @IBOutlet weak var Phone3TextField: UITextField!
    @IBOutlet weak var WebSiteView: UIView!
    @IBOutlet weak var WebSiteTextField: UITextField!
    @IBOutlet weak var DescriptionView: UIView!
    @IBOutlet weak var DescriptionTextView: UITextView!
    @IBOutlet weak var Gallery: UICollectionView!
    @IBOutlet weak var StatusLabel: UILabel!
    @IBOutlet weak var SaveView: UIView!
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var DeleteView: UIView!
    @IBOutlet weak var DeleteButton: UIButton!
    @IBOutlet weak var CloseButton: UIButton!
    
    // MARK: Properties
    
    var club: FClub!
    var data = [FImage]()
    let imagePicker = UIImagePickerController()
    var clubImage = true
    var saving = false
            
    // MARK: Closures
        
    var closeHandler: (() -> Void)?
        
    // MARK: Controller's method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NameView.makeShadow()
        self.OwnerView.makeShadow()
        self.CityView.makeShadow()
        self.AddressView.makeShadow()
        self.Phone1View.makeShadow()
        self.Phone2View.makeShadow()
        self.Phone3View.makeShadow()
        self.WebSiteView.makeShadow()
        self.DescriptionView.makeShadow()
        
        let me = FManager.manager().me!
        self.imagePicker.delegate = self
        self.DeleteView.isHidden = true
        if self.club == nil {
            let me = FManager.manager().me!
            self.club = FSerializer.deserialize(FClub.self, [
                "Owner": me.Phone
            ])!
//            self.club.Status.Status = .waitingForApproval
            self.club.Status.Status = .approved
        }
        else {
            self.DeleteView.isHidden = !(self.club.Owner == me.Phone)
        }
        
        refresh()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Utils.appDelegate().becomeView = self
    }
    
    // MARK: BecomeAppProtocol
    
    func become(_ changes: [FProtocol.Type], _ value: Any?) {
        if self.saving {
            return
        }
        
        if changes.contains(where: { $0 == FProfile.self || $0 == FClub.self }) {
            let clubs = FManager.manager().clubs
            if let c = clubs.first(where: { $0.key == self.club.Id }) {
                self.club = c.value
                self.refresh()
            }
        }
    }
    
    // MARK: UITextFieldDelegate, UITextViewDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if let contentOffset = textField.superview?.frame.minY {
            if contentOffset > self.ScrollView.bounds.height/2.0 {
                self.ScrollView.setContentOffset(CGPoint(x: 0.0, y: contentOffset - 40.0), animated: true)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if let contentOffset = textView.superview?.frame.minY {
            if contentOffset > self.ScrollView.bounds.height/2.0 {
                self.ScrollView.setContentOffset(CGPoint(x: 0.0, y: contentOffset - 40.0), animated: true)
            }
        }
    }
    
    // MARK: Methods
    
    func refresh() {
        
        let me = FManager.manager().me!
        
        self.NameTextField.text = self.club?.Name
        self.OwnerTextField.text = "UNKNOWN_NAME_STRING".localized
        self.CityTextField.text = self.club?.City
        self.AddressTextView.text = self.club?.Address
        self.Phone1TextField.text = self.club?.Phone1
        self.Phone2TextField.text = self.club?.Phone2
        self.Phone3TextField.text = self.club?.Phone3
        self.WebSiteTextField.text = self.club?.WebSite
        self.DescriptionTextView.text = self.club?.Description
        self.StatusLabel.text = self.club.Status.Status.toString()
        
        self.NameTextField.isEnabled = me.Phone == self.club.Owner
        self.OwnerTextField.isEnabled = false
        self.CityTextField.isEnabled = me.Phone == self.club.Owner
        self.AddressTextView.isEditable = me.Phone == self.club.Owner
        self.Phone1TextField.isEnabled = me.Phone == self.club.Owner
        self.Phone2TextField.isEnabled = me.Phone == self.club.Owner
        self.Phone3TextField.isEnabled = me.Phone == self.club.Owner
        self.WebSiteTextField.isEnabled = me.Phone == self.club.Owner
        self.DescriptionTextView.isEditable = me.Phone == self.club.Owner
        
        self.SaveView.isHidden = !(self.club.Owner == me.Phone)
        
        Utils.setImageFromCachOrDownload(self.club?.Image.Url, imageView: self.ClubImageView, completedHandler: nil)
        FManager.manager().getProfiles([self.club?.Owner].compactMap({ $0 }), completed: { (profiles) in
            if let owner = profiles.first {
                self.OwnerTextField.text = owner.DisplayName
            }
        })
        
        refreshData()
        self.ScrollView.contentSize = CGSize(width: self.ScrollView.bounds.width, height: self.SaveView.frame.maxY + 10.0)
    }
    
    func refreshData() {
        
        self.data.removeAll()
        self.data = self.club.Gallery.map({ $0.value }).sorted(by: { (f, s) -> Bool in
            f.Created < s.Created
        })
        
        self.Gallery.reloadData()
    }
    
    func deletePhoneFromGallery(_ fimage: FImage) {
        
        if self.club.Id.isEmpty {
            self.club.Gallery.removeValue(forKey: fimage.Id)
            refresh()
        }
        else {
            Utils.showProgress(self)
            self.club.deleteGalleryImage(fimage) { [unowned self] (error) in
                Utils.hideProgress(self)
                Utils.showErrorIfNeeded(error)
            }
        }
    }
    
    func hideKeyboard() {
        
        self.NameTextField.resignFirstResponder()
        self.OwnerTextField.resignFirstResponder()
        self.CityTextField.resignFirstResponder()
        self.AddressTextView.resignFirstResponder()
        self.Phone1TextField.resignFirstResponder()
        self.Phone2TextField.resignFirstResponder()
        self.Phone3TextField.resignFirstResponder()
        self.WebSiteTextField.resignFirstResponder()
        self.DescriptionTextView.resignFirstResponder()
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        
        dismiss(animated: true, completion: { [unowned self] in
            
            /*!!!guard let imageController = self.storyboard!.instantiateViewController(withIdentifier: "processImageViewController") as? ProcessImageViewController else {
                Utils.showError("INTERNAL_ERROR_STRING".localized)
                return
            }*/
            
            let fixedImage = pickedImage.fixedOrientation()
            /*!!!imageController.image = /*UIImage(named: "001")*/ fixedImage ?? pickedImage
            self.present(imageController, animated: true)
            
            imageController.closeHandler = { (image) in
                
                imageController.dismiss(animated: true, completion: nil)*/
                
                guard let image = /*image*/fixedImage else {
                    return
                }
                
                if self.clubImage {
                    self.ClubImageView.clearError()
                    self.ClubImageView.image = image
                    self.club.Image.Image = pickedImage
                    self.club.Image.Id = FManager.newKey()
                    
                    self.club.Image.uploadOperation = ClubImageUploadOperation()
                    self.club.Image.uploadOperation.getImage = { return self.club.Image }
                    self.club.Image.uploadOperation.getClub = { [unowned self] in return self.club }
                }
                else {
                    let fimage = FSerializer.deserialize(FImage.self, [:], FManager.newKey())!
                    fimage.Image = pickedImage
                    fimage.Club = self.club
                    
                    fimage.uploadOperation = GalleryImageUploadOperation()
                    fimage.uploadOperation.getImage = { return fimage }
                    fimage.uploadOperation.getClub = { [unowned self] in return self.club }
                    
                    if self.club.Id.isEmpty {
                        self.club.Gallery[fimage.Id] = fimage
                        self.refreshData()
                    }
                    else {
                        Utils.showProgress(self)
                        self.club.addGalleryImage(fimage) { [unowned self] in
                            Utils.hideProgress(self)
                        }
                    }
                }
            //!!!}
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return indexPath.row < self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        
         if action == #selector(delete(_:)) {
             return true
         }
         return false
    }

    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
        if action == #selector(delete(_:)) {
            let fimage = self.data[indexPath.row]
            self.deletePhoneFromGallery(fimage)
        }
    }
    
    @available(iOS 13.0, *)
    func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        if indexPath.row >= self.data.count {
            return nil
        }

        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { suggestedActions in

            // Here we specify the "destructive" attribute to show that it’s destructive in nature
            let delete = UIAction(title: "Delete Photo", image: UIImage(systemName: "trash"), attributes: .destructive) { [unowned self] action in
                // Delete this photo 😢
                let fimage = self.data[indexPath.row]
                self.deletePhoneFromGallery(fimage)
            }

            // Create our menu with both the edit menu and the share action
            return UIMenu(title: "Main Menu", children: [delete])
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/2.0 - 1.0, height: collectionView.bounds.height/2.0 - 1.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count + 1 // plus "add" button
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCellView", for: indexPath) as? GalleryCellView else {
            fatalError("Wrong cell")
        }
        
        cell.image = nil
        if indexPath.row < self.data.count {
            cell.image = self.data[indexPath.row]
        }
        cell.show()
        
        cell.showHandler = nil
        cell.showHandler = { [unowned self] (image) in
            print("show")
        }
        
        cell.addHandler = nil
        cell.addHandler = { [unowned self] in
            
            Utils.showConfirm("SELECT_MEDIA_SOURCE_STRING".localized, yesText: "CAMERA_STRING".localized, cancelText: "PHOTO_LIBRARY_STRING".localized, above: self) { [unowned self] (camera) in
                
                #if targetEnvironment(simulator)
                self.imagePicker.sourceType = .photoLibrary //.library
                #else
                self.imagePicker.sourceType = camera ? .camera : .photoLibrary //.camera
                
                if camera {
                    self.imagePicker.cameraDevice = .rear
                }
                #endif
                
                self.clubImage = false
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .photoLibrary //.library
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        
        cell.deleteHandler = nil
        cell.deleteHandler = { [unowned self] (fimage) in
            self.deletePhoneFromGallery(fimage)
        }
        
        return cell
    }
    
    // MARK: Actions
    
    @IBAction func tapClicked(_ recognizer: UITapGestureRecognizer) {
        hideKeyboard()
        
        let point = recognizer.location(in: self.ScrollView)
        if self.ClubImageView.frame.contains(point) {
            
            Utils.showConfirm("SELECT_MEDIA_SOURCE_STRING".localized, yesText: "CAMERA_STRING".localized, cancelText: "PHOTO_LIBRARY_STRING".localized, above: self) { [unowned self] (camera) in
                
                #if targetEnvironment(simulator)
                self.imagePicker.sourceType = .savedPhotosAlbum //.library
                #else
                self.imagePicker.sourceType = camera ? .camera : .savedPhotosAlbum
                
                if camera {
                    self.imagePicker.cameraDevice = .rear
                }
                #endif
                
                self.clubImage = true
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        
        self.ClubImageView.clearError()
        self.NameView.clearError()
        self.OwnerView.clearError()
        self.CityView.clearError()
        self.AddressView.clearError()
        self.Phone1View.clearError()
        self.DescriptionView.clearError()
        
        if self.club.Image.IsEmpty {
            self.ClubImageView.showError(self.ScrollView)
            Utils.showError("ENTER_IMAGE_STRING".localized, above: self)
            return
        }
        if let text = self.NameTextField.text, text.isEmpty {
            self.NameView.showError(self.ScrollView)
            Utils.showError("ENTER_CLUBNAME_STRING".localized, above: self)
            return
        }
        if let text = self.OwnerTextField.text, text.isEmpty {
            self.OwnerView.showError(self.ScrollView)
            Utils.showError("ENTER_OWNERNAME_STRING".localized, above: self)
            return
        }
        if let text = self.CityTextField.text, text.isEmpty {
            self.CityView.showError(self.ScrollView)
            Utils.showError("ENTER_CITY_STRING".localized, above: self)
            return
        }
        if let text = self.AddressTextView.text, text.isEmpty {
            self.AddressTextView.showError(self.ScrollView)
            Utils.showError("ENTER_ADDRESS_STRING".localized, above: self)
            return
        }
        if let text = self.Phone1TextField.text, text.isEmpty {
            self.Phone1View.showError(self.ScrollView)
            Utils.showError("ENTER_PHONE_STRING".localized, above: self)
            return
        }
        if let text = self.DescriptionTextView.text, text.isEmpty {
            self.DescriptionView.showError(self.ScrollView)
            Utils.showError("ENTER_DESCRIPTION_STRING".localized, above: self)
            return
        }
        
        let me = FManager.manager().me!
        
        self.club.Name = self.NameTextField.text?.trim() ?? ""
        self.club.Owner = me.Phone
        self.club.City = self.CityTextField.text?.trim() ?? ""
        self.club.Address = self.AddressTextView.text.trim()
        self.club.Phone1 = self.Phone1TextField.text?.trim() ?? ""
        self.club.Phone2 = self.Phone2TextField.text?.trim() ?? ""
        self.club.Phone3 = self.Phone3TextField.text?.trim() ?? ""
        self.club.WebSite = self.WebSiteTextField.text?.trim() ?? ""
        self.club.Description = self.DescriptionTextView.text
        
        self.club.Subscribers[me.Phone] = FSerializer.deserialize(FStatus.self, [
            "Status": EStatus.admin.rawValue
        ])
        
        self.saving = true
        let presenter = Utils.showProgressWithPersents(self)
        me.saveClub(self.club, progressHandler: { (progress) in
            print("total progress \(progress)")
            presenter.0(progress)
        }) { (error) in
            Utils.hideProgress(self)
            if let error = error {
                self.saving = false
                Utils.showErrorIfNeeded(error)
            }
            else {
                self.closeHandler?()
            }
        }
    }
    
    @IBAction func deleteClicked(_ sender: Any) {
        
        let me = FManager.manager().me!
        Utils.showConfirm("DELETE_CLUB_STRING".localized) { (yes) in
            if yes {
        
                self.saving = true
                Utils.showProgress(self)
                me.deleteClub(self.club) { (error) in
                    Utils.hideProgress(self)
                    if let error = error {
                        self.saving = false
                        Utils.showErrorIfNeeded(error)
                    }
                    else {
                        self.closeHandler?()
                    }
                }
            }
        }
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.closeHandler?()
    }
}

//
//  INetworkable.swift
//  Pocket.CEO
//
//  Created by MacOS on 22.04.2020.
//  Copyright © 2020 Vladimir Lozhnikov. All rights reserved.
//

import Foundation

protocol INetworkable {
    
    // MARK: Properties
    
    var UniqueId: String { get }
    var Name: String { get set}
    var currentProgress: Float { get set }
    var isCompleted: Bool { get }
    
    // MARK: Closures
    
    var getImage: (() -> FImage)? { get set }
    var getClub: (() -> FClub?)? { get set }
    
    // MARK: Methods
    
    func asyncNetworkAction(_ completed: ((Error?) -> Void)?)
}

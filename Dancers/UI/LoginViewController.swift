//
//  LoginViewController.swift
//  Pocket.CEO
//
//  Created by Владимир on 01.04.2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import DropDown
import PhoneNumberKit
import SwiftyTimer

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var LoginTitleLabel: UILabel!
    @IBOutlet weak var LoginTextLabel: UILabel!
    @IBOutlet weak var PhoneView: UIView!
    @IBOutlet weak var PhoneTextField: UITextField!
    @IBOutlet weak var PrefixCodeLabel: UILabel!
    @IBOutlet weak var FlagImageView: UIImageView!
    @IBOutlet weak var AccessCodeView: UIView!
    @IBOutlet weak var CodeView: UIView!
    @IBOutlet weak var CodeTextField: UITextField!
    @IBOutlet weak var JoinView: UIView!
    @IBOutlet weak var RequestButton: UIButton!
    @IBOutlet weak var LoginButton: UIButton!
    
    // MARK: Properties
    
    // MARK: Properties
    
    var verificationId: String!
    let phoneNumberKit = PhoneNumberKit()
    
    let countriesDropDown = DropDown()
    var prefixCode: String!
    var selectedCountry: FCountry!
    var countries: [FCountry] = []
    var counter = 0
    
    // MARK: Closures
    
    // MARK: Controller's methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Utils.showProgress(self)
        
        self.CodeView.makeShadow()
        self.PhoneView.makeShadow()
        
        self.ScrollView.isScrollEnabled = false
        
        // check credentionals
        let credentionals = getCredentionals()
        initCountriesDropDown(credentionals.prefix)
        
        if credentionals.prefix != nil && credentionals.code != nil && credentionals.phone != nil && credentionals.ver != nil {
            
            self.prefixCode = credentionals.prefix
            self.PrefixCodeLabel.text = self.prefixCode
            //self.PhoneTextField.text = credentionals.phone
            self.CodeTextField.text = credentionals.code
            self.verificationId = credentionals.ver!
            
            let formattedPhone = PartialFormatter().formatPartial(credentionals.prefix! + credentionals.phone!).replace(credentionals.prefix!, "")
            self.PhoneTextField.text = formattedPhone as String
            self.selectedCountry = self.countries.first(where: { $0.Code == credentionals.prefix })
            
            tryLogin()
        }
        else {
            Utils.hideProgress(self)
        }
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.PhoneTextField {
            self.ScrollView.setContentOffset(CGPoint(x: 0.0, y: self.PhoneView.frame.minY - 280.0), animated: true)
        }
        else if textField == self.CodeTextField {
            self.ScrollView.setContentOffset(CGPoint(x: 0.0, y: self.AccessCodeView.frame.minY - 150.0), animated: true)
        }
        
        // move up request code button
//        let yDiff = self.AccessCodeView.frame.maxY - self.JoinView.frame.minY + 30.0
//        UIView.animate(withDuration: 0.7) {
//            self.JoinView.transform = CGAffineTransform(translationX: 0.0, y: yDiff)
//        }
    }
    
    // MARK: Methods
    
    func logout() {
        
        self.clearCredentilnals()
        FManager.manager().myPhone = ""
    }
    
    func tryLogin() {
        
        //!!!Utils.showProgress(self.view)
        
        if let _ = FManager.manager().myPhone {
            show()
        }
        else {
            Utils.hideProgress(self)
            self.clearCredentilnals()
            FManager.manager().myPhone = ""
            Utils.showError("LOGIN_FAILED_STRING".localized, above: self)
        }
    }
    
    func initCountriesDropDown(_ prefix: String?) {
        
        let russia = FSerializer.deserialize(FCountry.self, [
            "Code": "7"
            ,"Name": "Russia"
            ,"DisplayName": "Russia"
            ], "Russia")!
        let belarus = FSerializer.deserialize(FCountry.self, [
            "Code": "375"
            ,"Name": "Belarus"
            ,"DisplayName": "Belarus"
            ], "Belarus")!
        
        russia.Flag = "ru"
        belarus.Flag = "by"
        self.FlagImageView.image = UIImage(named: belarus.Flag)
        
        self.countries = [russia, belarus]
        self.prefixCode = prefix ?? "+\(belarus.Code)"
        self.PrefixCodeLabel.text = self.prefixCode
        
        if let c = self.countries.first(where: { $0.Code == self.prefixCode }) {
            self.FlagImageView.image = UIImage(named: c.Flag)
            self.selectedCountry = c
        }
        
        self.countriesDropDown.dataSource = self.countries.map({ "+\($0.Code), \($0.DisplayName)" })
        self.countriesDropDown.anchorView = self.PhoneView
        
        self.countriesDropDown.cellNib = UINib(nibName: "CountryViewCell", bundle: nil)
        self.countriesDropDown.customCellConfiguration = { [unowned self] (index, item, cell) in
            
            guard let cell = cell as? CountryViewCell else { return }
            
            let country = self.countries[index]
            cell.optionLabel.text = "+\(country.Code), \(country.DisplayName)"
            cell.LogoImageView.image = UIImage(named: country.Flag)
        }
        
        self.countriesDropDown.selectionAction = nil
        self.countriesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            let country = self.countries[index]
            self.selectedCountry = country
            
            self.prefixCode = "+\(country.Code)"
            self.PrefixCodeLabel.text = self.prefixCode
            self.FlagImageView.image = UIImage(named: country.Flag)
            
            self.PhoneTextField.validatePhone(with: self.prefixCode ?? "", errorText: "ENTER_CELL_PHONE_NUMBER_STRING".localized)
        }
    }
    
    func showProfileSettings() {
        
        let appDelegate = Utils.appDelegate()
        
        let me = FManager.manager().me!
        let defaults = UserDefaults.standard
        let firstRun = defaults.value(forKey: "firstRun")
        
        if !me.IsEmpty && firstRun != nil {
            appDelegate.showMainForm()
        }
        else {
            
            let profileSettings = self.storyboard!.instantiateViewController(withIdentifier: "profileSettingsViewController") as! ProfileSettingsViewController
            
            profileSettings.modalPresentationStyle = .fullScreen
            profileSettings.closeHandler = {
                
                profileSettings.dismiss(animated: true) {
                    
                    appDelegate.showMainForm()
                }
            }
            
            self.present(profileSettings, animated: true, completion: nil)
        }
    }
        
    func hideKeyboard() {
        self.PhoneTextField.resignFirstResponder()
        self.CodeTextField.resignFirstResponder()
        self.ScrollView.setContentOffset(.zero, animated: true)
        
        UIView.animate(withDuration: 0.7) {
            self.JoinView.transform = CGAffineTransform.identity
        }
    }
    
    func saveCredentionals() {
        let defaults = UserDefaults.standard
        
        let phone = PhoneTextField.text!.trim().replace(" ", "").replace("-", "").replace(".", "").replace("(", "").replace(")", "")
        let code = self.CodeTextField.text?.trim()
        
        defaults.set(self.prefixCode, forKey: "prefix")
        defaults.set(phone, forKey: "phone")
        defaults.set(self.verificationId, forKey: "ver")
        defaults.set(code, forKey: "code")
    }
    
    func clearCredentilnals() {
        let defaults = UserDefaults.standard
        
        defaults.removeObject(forKey: "prefix")
        defaults.removeObject(forKey: "phone")
        defaults.removeObject(forKey: "code")
        defaults.removeObject(forKey: "ver")
    }
    
    func getCredentionals() -> (prefix: String?, phone: String?, code: String?, ver: String?) {
        let defaults = UserDefaults.standard
        
        let prefix = defaults.string(forKey: "prefix")
        var phone = defaults.string(forKey: "phone")
        let code = defaults.string(forKey: "code")
        let ver = defaults.string(forKey: "ver")
        
        if prefix != nil && phone != nil {
            
            phone = phone!.replace(" ", "").replace("-", "").replace(".", "").replace("(", "").replace(")", "")
            FManager.manager().myPhone = "\(prefix!)\(phone!)"
        }
        
        return (prefix: prefix, phone: phone, code: code, ver: ver)
    }
    
    func show() {
        
        FManager.manager().initilization(self.selectedCountry?.Id) { [unowned self] in
            
            Utils.hideProgress(self)
            UIView.animate(withDuration: 0.4, animations: {
                                
                self.view.alpha = 0.4
            }) { (done) in
                
                Utils.hideProgress(self)
                let me = FManager.manager().me!
                if me.IsEmpty {
                    self.showProfileSettings()
                }
                else {
                    Utils.appDelegate().showMainForm()
                }
            }
        }
    }
    
    // MARK: Actions
    
    @IBAction func tapClicked(_ recognizer: UITapGestureRecognizer) {
        hideKeyboard()
        
        let point = recognizer.location(in: self.PhoneView)
        if self.PrefixCodeLabel.frame.contains(point) {
            self.countriesDropDown.show()
        }
    }
    
    @IBAction func phoneChanged(_ sender: Any) {
        
        let prefix = self.prefixCode ?? ""
        let phoneText = "\(prefix)\(self.PhoneTextField.text ?? "")"
        
        let formattedPhone = PartialFormatter().formatPartial(phoneText).replace(prefix, "")
        self.PhoneTextField.text = formattedPhone as String
        
        if let _ = self.phoneNumberKit.parse([phoneText]).first {
            
            self.PhoneTextField.clearError()
            self.hideKeyboard()
        }
    }
    
    @IBAction func codeChanged(_ sender: Any) {
        
        if let t = self.CodeTextField.text, t.count >= 5 {
            self.CodeTextField.clearError()
        }
    }
    
    @IBAction func getCodeClicked(_ sender: Any) {
        
        if !self.PhoneTextField.validatePhone(with: self.prefixCode ?? "", errorText: "ENTER_CELL_PHONE_NUMBER_STRING".localized)
        {
            return
        }
        
        hideKeyboard()
        
        // get login code
        Utils.showProgress(self.view)
        
        // get full phone number
        let phone = self.prefixCode + self.PhoneTextField.text!.trim().replace(" ", "").replace("-", "").replace(".", "").replace("(", "").replace(")", "")
        self.verificationId = nil
        
//        Auth.auth().settings?.isAppVerificationDisabledForTesting = true
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { [unowned self] (vId, error) in
            
            Utils.hideProgress(self.view)
            
            if error == nil {
                
                Utils.showInfo("CODE_SENT_SMS_STRING".localized, above: self) {
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        
                        self.JoinView.alpha = 0.0
                    }) { [unowned self] (done) in
                        
                        self.JoinView.alpha = 0.0
                        self.JoinView.isHidden = false
                        
                        self.AccessCodeView.alpha = 0.0
                        self.AccessCodeView.isHidden = false
                        
                        self.verificationId = vId
                        
                        UIView.animate(withDuration: 0.4, animations: {
                            
                            self.AccessCodeView.alpha = 1.0
                            self.JoinView.alpha = 1.0
                        }) { [unowned self] (done) in
                            self.ScrollView.isScrollEnabled = true
                            self.LoginButton.isHidden = false
                            self.CodeTextField.becomeFirstResponder()
                            
                            let modelName = UIDevice().modelName.lowercased().replace(" ", "")
                            if modelName.contains("iphone5") {
                                self.ScrollView.isScrollEnabled = true
                                self.JoinView.frame.origin.y = self.AccessCodeView.frame.maxY + 20.0
                            }
                            self.ScrollView.contentSize = CGSize(width: self.ScrollView.bounds.width, height: self.JoinView.frame.maxY + 50.0)
                        }
                    }
                }
            }
            else {
                Utils.showInfo(error!.localizedDescription, above: self)
            }
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        hideKeyboard()
        UIView.animate(withDuration: 0.4, animations: {
            
            self.AccessCodeView.alpha = 0.0
        }) { (done) in
            
            self.AccessCodeView.isHidden = true
            self.AccessCodeView.alpha = 1.0
            
            self.JoinView.alpha = 0.0
            self.JoinView.isHidden = false
            self.ScrollView.isScrollEnabled = false
            
            UIView.animate(withDuration: 0.4, animations: {
                
                self.JoinView.alpha = 1.0
            })
        }
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        
        hideKeyboard()
        if !self.CodeTextField.validate(errorText: "TRY_AGAIN_STRING".localized) {
            Utils.showError("ENTER_CODE_STRING".localized, above: self)
            return
        }
        
        let code = self.CodeTextField.text!.trim()
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.verificationId!, verificationCode: code)
        
        Utils.showProgress(self)
        Auth.auth().signIn(with: credential) { [unowned self] (authData, error) in
            
            if error != nil {
                Utils.hideProgress(self)
                Utils.showError("LOGIN_FAILED_STRING".localized, above: self)
                self.clearCredentilnals()
                return
            }
            
            FManager.manager().myPhone = authData!.user.phoneNumber
            self.saveCredentionals()
            self.show()
        }
    }
}

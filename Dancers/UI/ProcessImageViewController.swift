//
//  ProcessImageViewController.swift
//  Pocket.CEO
//
//  Created by MacOS on 24.02.2020.
//  Copyright © 2020 Vladimir Lozhnikov. All rights reserved.
//

import UIKit

class UITargetView: UIView {
    
    // MARK: Outlets
    
    // MARK: Closures
    
    var resizeHandler: ((_ rect: CGRect) -> Void)?
    
    // MARK: Properties
    
    // MARK: Methods
    
    func show() {
        
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = self.bounds.width/2.0
    }
    
    // MARK: Actions
}

class ProcessImageViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var ImageView2: UIImageView!
    @IBOutlet weak var TargetView: UITargetView!
    
    // MARK: Properties
    
    enum SelectMode {
        case background
        case target
    }
    
    var pinchGesture: UIPinchGestureRecognizer!
    var panGesture: UIPanGestureRecognizer!
    var tapGesture: UITapGestureRecognizer!
    var tapModeGesture: UITapGestureRecognizer!
    
    var initialCenter = CGPoint()
    var mode: SelectMode = .background
    var panPiece: UIView!
    var image: UIImage!
    
    // MARK: Closures
    
    var closeHandler: ((_ image: UIImage?) -> Void)?
        
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchHandler(_:)))
        self.view.addGestureRecognizer(self.pinchGesture)
        
        self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panHandler(_:)))
        self.view.addGestureRecognizer(self.panGesture)
        
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapHandler(_:)))
        self.tapGesture.numberOfTapsRequired = 2
        self.TargetView.addGestureRecognizer(self.tapGesture)
        
        self.tapModeGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapModeHandler(_:)))
        self.view.addGestureRecognizer(self.tapModeGesture)
        
        var rect = self.TargetView.frame
        rect.size.width = self.ImageView2.bounds.size.width*0.7
        rect.size.height = rect.size.width
        self.TargetView.frame = rect
        self.TargetView.center = self.ImageView2.center
        self.TargetView.show()
        
        self.ImageView2.image = self.image
        
//        updateSelect()
    }
    
    // MARK: Methods
    
    func updateSelect() {
        
        if self.mode == .background {
            self.ImageView2.select(0.0, UIColor.clear)
            self.TargetView.unselect(5.0, UIColor.green)
        }
        else {
            self.ImageView2.unselect(0.0, UIColor.clear)
            self.TargetView.select(5.0, UIColor.green)
        }
    }
    
    // MARK: Actions
    
    @objc func tapModeHandler(_ gestureRecognizer : UITapGestureRecognizer) {
            
            guard gestureRecognizer.view != nil else { return }
    
            let tapPoint = gestureRecognizer.location(in: self.view)
            if self.TargetView.frame.contains(tapPoint) {
                self.mode = .target
            }
            else {
                self.mode = .background
            }
    
//            updateSelect()
    }
    
    @objc func tapHandler(_ gestureRecognizer : UITapGestureRecognizer) {
        
        guard let img = self.ImageView2.image else {
            return
        }
        
        var rect = self.TargetView.frame
        let scale = self.ImageView2.transform.scale
        
        let kh = img.size.height/self.ImageView2.bounds.height
        let canvasWidth = img.size.width/kh
        
        let offsetx = self.ImageView2.frame.origin.x
        let kx = img.size.width/canvasWidth
        
        let offsety = self.ImageView2.frame.origin.y
        let ky = img.size.height/self.ImageView2.bounds.height
        
        let deltaCenter = (canvasWidth-self.ImageView2.bounds.width)/2.0
        
        self.ImageView2.transform = (self.ImageView2.transform.scaledBy(x: 1.0/scale, y: 1.0/scale))
        
        let x2 = (self.TargetView.frame.origin.x*(1/scale) - offsetx*(1/scale) + deltaCenter)*kx
        let y2 = (self.TargetView.frame.origin.y - offsety)*(1/scale)*ky
        
//        print("x: \(x2), y: \(y2)")
                
        rect.origin.x = x2
        rect.origin.y = y2
        rect.size.width *= (min(kx, ky)*(1/scale))
        rect.size.height = rect.size.width
        
        if rect.origin.x < 0.0 {
            rect.origin.x = 0.0
        }
        else if rect.origin.x + rect.size.width > img.size.width {
            rect.origin.x = img.size.width - rect.size.width
        }
        if rect.origin.y < 0.0 {
            rect.origin.y = 0.0
        }
        else if rect.origin.y + rect.size.height > img.size.height {
            rect.origin.y = img.size.height - rect.size.height
        }
        
        let croppedImage = img.crop(rect: rect)
        
        self.ImageView2.image = nil
        self.ImageView2.image = croppedImage
        
        let oldRadius = self.ImageView2.layer.cornerRadius
        let oldBorder = self.ImageView2.layer.borderWidth
        let oldRect = self.ImageView2.frame
        
        self.ImageView2.layer.cornerRadius = self.ImageView2.bounds.width/2.0
        self.ImageView2.layer.borderWidth = 0.0
        self.ImageView2.clipsToBounds = true
        self.ImageView2.frame.size.height = self.ImageView2.frame.size.width
        self.ImageView2.center = self.view.center

//        let path = croppedImage.save(in: "img")
//        print(path!.path)

        self.TargetView.isHidden = true
        Timer.after(2.0) {
            DispatchQueue.main.async {
        
                self.ImageView2.transform = (self.ImageView2.transform.scaledBy(x: 1.0, y: 1.0))
                self.ImageView2.layer.cornerRadius = oldRadius
                self.ImageView2.layer.borderWidth = oldBorder
                self.ImageView2.clipsToBounds = false
                self.ImageView2.frame = oldRect
                
                self.TargetView.isHidden = false
                self.ImageView2.image = self.image
                Utils.showConfirm("CONTINUE_STRING".localized, yesText: "YES_STRING".localized, cancelText: "REWORK_STRING".localized, above: self) { (yes) in
                    if yes {
                        self.closeHandler?(croppedImage)
                    }
                }
            }
        }
    }
    
    @objc func panHandler(_ gestureRecognizer : UIPanGestureRecognizer) {
        
        guard gestureRecognizer.view != nil else {return}
        
        // Get the changes in the X and Y directions relative to
        // the superview's coordinate space.
        if gestureRecognizer.state == .began {
            
            self.panPiece = self.ImageView2!
            let tapPoint = gestureRecognizer.location(in: self.view)
            if self.TargetView.frame.contains(tapPoint) {
                self.panPiece = self.TargetView!
            }
            
            // Save the view's original position.
            self.initialCenter = self.panPiece.center
        }
           // Update the position for the .began, .changed, and .ended states
        if gestureRecognizer.state != .cancelled {
            
            let translation = gestureRecognizer.translation(in: self.panPiece.superview)
            // Add the X and Y translation to the view's original position.
            let newCenter = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y + translation.y)
            self.panPiece.center = newCenter
        }
        else {
            // On cancellation, return the piece to its original location.
            self.panPiece.center = self.initialCenter
        }
    }
    
    @objc func pinchHandler(_ gestureRecognizer : UIPinchGestureRecognizer) {
        
        guard gestureRecognizer.view != nil else { return }
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            
            let piece = self.mode == .target ? self.TargetView! : self.ImageView2!
//            let piece = self.TargetView!
//            let piece = self.ImageView2!
            piece.transform = (piece.transform.scaledBy(x: gestureRecognizer.scale, y: gestureRecognizer.scale))
            
            gestureRecognizer.scale = 1.0
        }
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        
        closeHandler?(nil)
    }
}

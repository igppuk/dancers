//
//  JobOperation.swift
//  Pocket.CEO
//
//  Created by MacOS on 23.04.2020.
//  Copyright © 2020 Vladimir Lozhnikov. All rights reserved.
//

import Foundation
import FirebaseUI

class GalleryImageUploadOperation: INetworkable {
    
    
    // MARK: Properties
    
    var UniqueId: String {
        get {
            if let fimage = self.getImage?() {
                return fimage.Id
            }
            
            return ""
        }
    }
    var Name = "Gallery Image Upload Job"
    var currentProgress = Float(0.0)
    var isCompleted: Bool {
        get {
            return self.currentProgress >= 100.0
        }
    }
    
    // MARK: Closures
    
    var getImage: (() -> FImage)?
    var getClub: (() -> FClub?)?
    
    // MARK: Methods
    
    func asyncNetworkAction(_ completed: ((Error?) -> Void)?) {
        
        if self.isCompleted {
            completed?(nil)
            return
        }
        
        guard let fimage = self.getImage?() else {
            completed?(NSError(domain:"image not found", code:1, userInfo:nil))
            return
        }
        
        guard let club = self.getClub?() else {
            completed?(NSError(domain:"club not found", code:2, userInfo:nil))
            return
        }
        
        if let imageData = fimage.tempImageData {
            
            // upload image
            let imageName = "\(FManager.newKey()).jpg"
            
            fimage.Name = imageName
            let totalCount = imageData.count
                        
            // upload new image
            let storageRef = Storage.storage().reference().child("Clubs/\(club.Id)/Gallery/\(fimage.Name)")
            storageRef.putData(imageData, metadata: nil) { (metadata, error) in
                
                storageRef.downloadURL(completion: { (url, error1) in
                    
                    if error1 == nil {
                        
                        fimage.Url = url!.absoluteString
                    }
                    else {
                        print(error1?.localizedDescription)
                    }
                    self.currentProgress = 100.0
                })
            }.observe(.progress) { (snapshot) in
                if let p = snapshot.progress {
                    let unitCount = UInt64(p.completedUnitCount)
                    var cProgress = Float(unitCount) / Float(totalCount) * 100.0
                    if cProgress >= 99.0 {
                        cProgress = 99.0
                    }
                    self.currentProgress = cProgress
                }
            }
        }
    }
}

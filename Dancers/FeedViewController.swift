//
//  FeedViewController.swift
//  Dancers
//
//  Created by MacOS on 30.05.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit

private class LeftPresenter: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let toViewController = transitionContext.viewController(forKey: .to)!
        let containerView = transitionContext.containerView
        
        let animationDuration = transitionDuration(using: transitionContext)
        
        toViewController.view.transform = CGAffineTransform(translationX: -containerView.bounds.width, y: 0)
        toViewController.view.layer.shadowColor = UIColor.black.cgColor
        toViewController.view.layer.shadowOffset = CGSize(width: -4.0, height: 0.0)
        toViewController.view.layer.shadowOpacity = 0.3
        toViewController.view.layer.cornerRadius = 30.0
        toViewController.view.clipsToBounds = true
        
        containerView.addSubview(toViewController.view)
        
        UIView.animate(withDuration: animationDuration, animations: {
            toViewController.view.transform = CGAffineTransform(translationX: -containerView.bounds.width*0.15, y: 0)
        }) { (finished) in
            transitionContext.completeTransition(finished)
        }
    }
}

private class LeftDismisser: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let fromViewController = transitionContext.viewController(forKey: .from)
            else {
                return
        }

        let animationDuration = transitionDuration(using: transitionContext)

        //fromViewController.view.transform = CGAffineTransform(translationX: -fromViewController.view.bounds.width*0.15, y: 0)
        UIView.animate(withDuration: animationDuration, animations: {
            fromViewController.view.transform = CGAffineTransform(translationX: -fromViewController.view.bounds.width, y: 0)
        }) { (finished) in
            fromViewController.view.removeFromSuperview()
            transitionContext.completeTransition(finished)
        }
    }
}

class FeedTableViewCell: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var ProfileImageView: UIImageView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var MessageView: UIView!
    @IBOutlet weak var MessageLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var DeleteButton: UIButton!
    
    // MARK: Closures
    
    var deleteHandler: ((_ entry: FFeedEntry) -> Void)?
    
    // MARK: Properties
    
    weak var entry: FFeedEntry!
    
    // MARK: Methods
    
    func show() {
        
        let me = FManager.manager().me!
        self.NameLabel.text = nil
        self.ProfileImageView.image = nil
        self.MessageLabel.attributedText = nil
        self.DateLabel.text = nil
        
        let text = FeedTableViewCell.compileText(self.entry, self.MessageLabel)
        self.MessageLabel.attributedText = text
        self.DateLabel.text = self.entry.Created.formatted("MMM d, yyyy HH:mm:ss")
        self.DeleteButton.isHidden = !(me.Phone == entry.Owner)
        
        FManager.manager().getProfiles([self.entry.Owner]) { (profiles) in
            if let p = profiles.first {
                self.NameLabel.text = p.DisplayName
                Utils.setImageFromCachOrDownload(p.Image.Url, imageView: self.ProfileImageView, completedHandler: nil)
            }
        }
    }
    
    class func compileText(_ entry: FFeedEntry, _ label: UIView? = nil) -> NSAttributedString {
        
        let attrText = NSMutableAttributedString()
        entry.Text.forEach { (part) in
            if part.Image.IsEmpty {
                let text = NSAttributedString(string: part.Text)
//                attrText.insert(text, at: part.Location)
                attrText.append(text)
            }
            else {
                let attach = NSTextAttachment()
                let newImageWidth = CGFloat(70.0)
                attach.bounds = CGRect.init(x: 0, y: 0, width: newImageWidth, height: newImageWidth)
                let imageString = NSAttributedString(attachment: attach)
//                attrText.insert(imageString, at: part.Location)
                attrText.append(imageString)
                
                if label != nil {
                    Utils.setImageFromCachOrDownload(part.Image.Url, imageView: UIImageView()) { (image) in
                        attach.image = image
                        label!.setNeedsLayout()
                        label!.setNeedsDisplay()
                    }
                }
            }
        }
        
        return attrText
    }
    
    class func optimalHeight(_ entry: FFeedEntry, _ width: CGFloat) -> CGFloat {
        let attrText = FeedTableViewCell.compileText(entry)
        let height = attrText.heightWithConstrainedWidth(width: width)
        return height
    }
    
    // MARK: Actions
    
    @IBAction func deleteClicked(_ sender: Any) {
        self.deleteHandler?(self.entry)
    }
}

class FeedViewController: UIViewController, BecomeAppProtocol, UIViewControllerTransitioningDelegate, UITableViewDelegate, UITableViewDataSource {
        
    // MARK: Outlets
    
    @IBOutlet weak var LeftMenuButton: UIButton!
    @IBOutlet weak var FeedTableView: UITableView!
    @IBOutlet weak var FeedTitleLabel: UILabel!
    
    // MARK: Properties
    
    var leftMenuController: LeftMenuViewController!
    var data = [FClub]()
    
    // MARK: UIViewController methods

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // init left menu
        self.leftMenuController = storyboard!.instantiateViewController(withIdentifier: "leftMenuViewController") as? LeftMenuViewController
        self.leftMenuController.transitioningDelegate = self
        self.leftMenuController.modalPresentationStyle = .overCurrentContext
        
        self.leftMenuController.presentHandler = { [unowned self] in
            self.leftMenuClicked(self)
        }
        
        self.leftMenuController.dismissHandler = { [unowned self] in
            
            Utils.appDelegate().becomeView = self
            self.refresh()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Utils.appDelegate().becomeView = self
        
        refresh()
    }
    
    // MARK: BecomeAppProtocol
    
    func become(_ changes: [FProtocol.Type], _ value: Any?) {
        if changes.contains(where: { $0 == FClub.self }) {
            self.refresh()
        }
    }
    
    // MARK: Methods
    
    func refresh() {
        
        let me = FManager.manager().me!
        self.data.removeAll()
        
        let clubs = FManager.manager().clubs.map({ $0.value })
            .filter({ $0.Selected[me.Phone] == true })
            .sorted { (f, s) -> Bool in f.Name < s.Name }
        self.data.append(contentsOf: clubs)
        
        self.FeedTitleLabel.isHidden = self.data.count != 0
        self.FeedTableView.reloadData()
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data[section].Feed.Entries.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let club = self.data[indexPath.section]
        let entry = club.Feed.SortedEntries[indexPath.row]
        let height = FeedTableViewCell.optimalHeight(entry, 200.0) + 100.0
        
        return height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 180.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let club = self.data[section]
        
        let headerView = ClubViewCell.fromNib("ClubViewCell")
        headerView.Club = club
        headerView.show()
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let club = self.data[section]
        return club.Name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "feedTableViewCell", for: indexPath) as? FeedTableViewCell else {
            fatalError("wrong cell")
        }
        
        let club = self.data[indexPath.section]
        let entry = club.Feed.SortedEntries[indexPath.row]
        cell.entry = entry
        cell.show()
        
        cell.deleteHandler = nil
        cell.deleteHandler = { (entry) in
            
            Utils.showConfirm("DELETE_ENTRY_STRING".localized, above: self) { (yes) in
                if yes {
                    
                    Utils.showProgress(self)
                    club.deleteEntry(entry) { [unowned self] (error) in
                        Utils.hideProgress(self)
                        Utils.showErrorIfNeeded(error)
                    }
                }
            }
        }
        
        return cell
    }
    
    // MARK: UIViewControllerTransitioningDelegate
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if presented == self.leftMenuController {
            return LeftPresenter()
        }
        
        return nil
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        if dismissed == self.leftMenuController {
            return LeftDismisser()
        }
        
        return nil
    }
    
    // MARK: Actions
    
    @IBAction func leftMenuClicked(_ sender: Any) {
        
        present(self.leftMenuController, animated: true, completion: { [unowned self] in
           self.leftMenuController.refresh()
       })
    }
    
    @IBAction func swipeAction(_ recognizer: UISwipeGestureRecognizer) {
        
        if recognizer.direction == .right {
            present(self.leftMenuController, animated: true, completion: { [unowned self] in
                self.leftMenuController.refresh()
            })
        }
    }
}


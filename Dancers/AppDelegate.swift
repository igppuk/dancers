//
//  AppDelegate.swift
//  Dancers
//
//  Created by MacOS on 30.05.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseAuth
import FirebaseMessaging
import FirebaseCore
import DropDown

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var sceneWindow: UIWindow? {
        get {
            if #available(iOS 13.0, *) {
                return SceneDelegate.shared?.window
            } else {
                return self.window
            }
        }
        set {
            if #available(iOS 13.0, *) {
                SceneDelegate.shared?.window = newValue
            } else {
                self.window = newValue
            }
        }
    }
    
    var udid: String? = nil
    let cache = NSCache<AnyObject, AnyObject>()
    var becomeView: BecomeAppProtocol? = nil
    
    var activeTab: Int {
        set {
            guard let mainController = self.sceneWindow?.rootViewController as? MainViewController else {
                return
            }
            
            mainController.selectedIndex = newValue
        }
        get {
            guard let mainController = self.sceneWindow?.rootViewController as? MainViewController else {
                return 0
            }
            
            return mainController.selectedIndex
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        DropDown.startListeningToKeyboard()
        
        let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        //        #if DEBUG
        //        let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info-dev", ofType: "plist")
        //        #else
        //        let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        //        #endif

        guard let options = FirebaseOptions(contentsOfFile: firebaseConfig!) else {
            fatalError("Invalid Firebase configuration file.")
        }

        FirebaseApp.configure(options: options)
//        FirebaseApp.configure()
        
        // try restore imges and save they in cache
        restoreCache()
        
        // register push notifier
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { (granted, error) in
            if granted {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            }
        })
        
        Messaging.messaging().delegate = self
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        self.udid = fcmToken
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
                   
        Auth.auth().setAPNSToken(deviceToken, type: .sandbox) // or .prod
//        #if DEBUG
//        Auth.auth().setAPNSToken(deviceToken, type: .sandbox)
//        #else
//        Auth.auth().setAPNSToken(deviceToken, type: .prod)
//        #endif
        
        let token = deviceToken.map { String(format: "%.2hhx", $0) }.joined()
        print(token)
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let badge = userInfo["badge"] as? String, let intBadge = Int(badge) {
            Utils.showIconBadge(intBadge)
        }
        
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler(.noData)
            return
        }
        
        completionHandler(.newData)
    }
    
    // For iOS 9+
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      if Auth.auth().canHandle(url) {
        return true
      }
      // URL not auth related, developer should handle it.
        
        return true
    }

    // For iOS 8-
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
      if Auth.auth().canHandle(url) {
        return true
      }
      // URL not auth related, developer should handle it.
        
        return true
    }
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
      for urlContext in URLContexts {
          let url = urlContext.url
          Auth.auth().canHandle(url)
      }
      // URL not auth related, developer should handle it.
    }
    
    // this method calls when app is running
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        completionHandler([.alert, .badge, .sound])
    }
    
    // this method calls when user clicks on notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        restoreCache()
        removeOldVideos()
    }
    
    func restoreCache() {
        // try restore imges and save they in cache
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let fileManager = FileManager.default
        
        // remove extra images from disk
        let enumerator:FileManager.DirectoryEnumerator = fileManager.enumerator(atPath: paths)!
        while let element = enumerator.nextObject() as? String {
            
            if element.hasSuffix("jpg") { // checks the extension
                
                let filePathToWrite = "\(paths)/\(element)"
                let image = UIImage(contentsOfFile: filePathToWrite)
                
                if image != nil {
                    // save image in the cache
                    let cacheName = "\(NSURL(fileURLWithPath: filePathToWrite).deletingPathExtension!.lastPathComponent).jpg"
                    cache.setObject(image!, forKey: cacheName as AnyObject)
                }
            }
        }
    }
    
    func removeOldVideos() {
        
        // check the last removal date
        let me = FManager.manager().me
        let defaults = UserDefaults.standard
        
        var clear = true
        if let lastDate = defaults.object(forKey: "lastClearingDate") as? Date {
            
            let calendar = Calendar.current
            if let days = calendar.dateComponents([.day], from: lastDate, to: Date()).day {
                
                if abs(days) < 20 {
                    clear = false
                }
            }
        }
        
        if clear && me != nil {
            
            defaults.setValue(Date(), forKey: "lastClearingDate")
            
            if let _ = FManager.manager().myPhone {
                
                // find all unused videos
                let fileManager = FileManager.default
                
                do {
                    let documentDirectory = Utils.getDocumentsDirectory()
                    let directoryContents = try fileManager.contentsOfDirectory(at: documentDirectory, includingPropertiesForKeys: nil)
                    
                    let mp4Files = directoryContents.filter({ $0.pathExtension == "mp4" }).map({ $0.lastPathComponent })
                    print(mp4Files)
                    
//                    let properties = me!.SubscribeAndWaitingPropertiesWithoutFilter
//
//                    mp4Files.forEach { (mp4Name) in
//
//                        properties.forEach { (property) in
//
//                            // checking video
//                            let first1 = property.Jobs.map({ $0.value }).first { (job) -> Bool in
//
//                                job.SortedVideos.contains { (fimage) -> Bool in
//
//                                    fimage.Name ==  mp4Name
//                                }
//                            }
//
//                            // checking video reply
//                            let first2 = property.Jobs.map({ $0.value }).first { (job) -> Bool in
//
//                                job.Statuses.contains { (js) -> Bool in
//                                    js.Video != nil && js.Video.Name == mp4Name
//                                }
//                            }
//
//                            if first1 == nil && first2 == nil {
//
//                                Utils.removeVideoFromCach(mp4Name)
//                            }
//                        }
//                    }
                }
                catch {
                    print(error)
                }
            }
        }
    }
    
    func showLoginForm() {
        
        if let loginController = self.sceneWindow?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as? LoginViewController {
            
            loginController.logout()
            self.sceneWindow?.rootViewController = loginController
            
            self.sceneWindow?.rootViewController?.view.alpha = 0.4
            self.sceneWindow?.makeKeyAndVisible()
            
            UIView.animate(withDuration: 0.4, animations: {
                self.sceneWindow?.rootViewController?.view.alpha = 1.0
            })
        }
        else {
            Utils.showError("INTERNAL_ERROR_STRING".localized)
        }
    }
    
    func showMainForm() {
        
        if let mainController = self.sceneWindow?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "mainViewController") as? MainViewController {
            
            if let me = FManager.manager().me {
                me.Device.Model = UIDevice().modelName
                me.Device.Udid = Utils.appDelegate().udid ?? me.Device.Udid
                me.save(completed: nil)
            }
            
            self.sceneWindow?.rootViewController = mainController
            
            self.sceneWindow?.rootViewController?.view.alpha = 0.4
            self.sceneWindow?.makeKeyAndVisible()
            
            UIView.animate(withDuration: 0.4, animations: {
                self.sceneWindow?.rootViewController?.view.alpha = 1.0
            })
        }
    }

    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}


//
//  CountryViewCell.swift
//  Pocket.CEO
//
//  Created by MacOS on 27/06/2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import UIKit
import DropDown

class CountryViewCell: DropDownCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var LogoImageView: UIImageView!
}

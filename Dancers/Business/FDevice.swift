//
//  FDevice.swift
//  Pocket.CEO
//
//  Created by Владимир on 01.04.2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import Foundation
import Firebase

class FDevice: FProtocol, Codable {
    
    var Id = ""
    var Model = ""
    var Udid = ""
    
    enum CodingKeys: String, CodingKey {
        
        case Model
        case Udid
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Model) {
            do {
                self.Model = try container.decode(String.self, forKey: .Model)
            }
            catch {
                print(error)
            }
        }
        if container.contains(.Udid) {
            do {
                self.Udid = try container.decode(String.self, forKey: .Udid)
            }
            catch {
                print(error)
            }
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
}

//
//  Utils.swift
//  Pocket.CEO
//
//  Created by Владимир on 01.04.2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import MBProgressHUD
import AVKit
import UserNotifications
import FirebaseStorage

class Utils {
    class func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    class func setImageFromCachOrDownload(_ url: String?, imageView: UIImageView, placeholderImage: UIImage? = nil, force: Bool = false, completedHandler: ((_ image: UIImage?) throws -> Void)?) -> Void {
        
        if url == nil {
            do {
                try completedHandler?(nil)
            }
            catch {
                print("object was deallocated 1")
            }
            return
        }
        
        guard let url = url, !url.isEmpty else {
            do {
                try completedHandler?(nil)
            }
            catch {
                print("object was deallocated 1")
            }
            return
        }
        let nsurl = NSURL(fileURLWithPath: url).deletingPathExtension
        
        var cacheName = ""
        let pcn = "\(url)_processing_"
        
        //TODO: fixing of memory leak
        if let nsurl = nsurl {
            cacheName = "\(nsurl.lastPathComponent).jpg"
        }
        
        let imageCach = Utils.appDelegate().cache.object(forKey: cacheName as NSString) as? UIImage
        
        if imageCach == nil || force {
            
            Utils.appDelegate().cache.setObject(NSNumber.init(value: true), forKey: pcn as NSString)
            
            imageView.kf.setImage(with: URL(string: url)!, placeholder: placeholderImage, options: [.transition(ImageTransition.fade(1)), .backgroundDecode], completionHandler: {
                result in
                
                switch result {
                case .success(let value):
                    
                    imageView.image = value.image
                    
                    // save image in file and cach
                    let fileManager = FileManager.default
                    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                    
                    // get file name
                    let filePathToWrite = "\(paths)/\(cacheName)"
                    let imageData: Data = value.image.jpegData(compressionQuality: 1.0)!
                    
                    // save image in the file
                    fileManager.createFile(atPath: filePathToWrite, contents: imageData, attributes: nil)
                    
                    // save in the cache
                    Utils.appDelegate().cache.setObject(value.image, forKey: cacheName as NSString)
                    Utils.appDelegate().cache.removeObject(forKey: pcn as NSString)
                    
                    do {
                        try completedHandler?(value.image)
                    }
                    catch {
                        print("object was deallocated 1")
                    }
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                    do {
                        try completedHandler?(nil)
                    }
                    catch {
                        print("object was deallocated 1")
                    }
                }
            })
        }
        else {
            if imageView.image != nil && imageCach != nil {
                let pcn2 = "\(Unmanaged<AnyObject>.passUnretained(imageView.image!).toOpaque())"
                let pcn3 = "\(Unmanaged<AnyObject>.passUnretained(imageCach!).toOpaque())"
                if pcn2 != pcn3 {
                    imageView.image = imageCach
                }
            }
            else if imageView.image == nil {
                imageView.image = imageCach
            }
            
            do {
                try completedHandler?(imageCach)
            }
            catch {
                print("object was deallocated 3")
            }
        }
    }
    
    class func removeVideoFromCach(_ fimage: FImage?) {
        if fimage == nil {
            return
        }
        
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let cacheName = fimage!.Name
        let filePathToWrite = URL(fileURLWithPath: "\(paths)/\(cacheName)")
        
        if fileManager.fileExists(atPath: filePathToWrite.path) {
            do {
                try FileManager.default.removeItem(at: filePathToWrite)
            } catch {
                print(error)
            }
        }
    }
    
    class func removeVideoFromCach(_ fimage: String?) {
        if fimage == nil {
            return
        }
        
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let cacheName = fimage!
        let filePathToWrite = URL(fileURLWithPath: "\(paths)/\(cacheName)")
        
        if fileManager.fileExists(atPath: filePathToWrite.path) {
            do {
                try FileManager.default.removeItem(at: filePathToWrite)
            } catch {
                print(error)
            }
        }
    }
    
    class func setVideoFromCachOrDownload(_ fimage: FImage?, progress: ((_ progress: Float) -> Void)?, completedHandler: ((_ url: URL?) -> Void)?) -> StorageDownloadTask? {
                
        if fimage == nil {
            completedHandler?(nil)
            return nil
        }
        
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let cacheName = fimage!.Name
        let filePathToWrite = URL(fileURLWithPath: "\(paths)/\(cacheName)")
        
        //print(filePathToWrite.absoluteString)
        if !fileManager.fileExists(atPath: filePathToWrite.path) {
            
            // download video from storage
            
            // save video in file and cach
            let storageRef = Storage.storage().reference(forURL: fimage!.Url)
            
            let downloadTask = storageRef.write(toFile: filePathToWrite) { url, error in
              if let error = error {
                print(error.localizedDescription)
              }
                
              completedHandler?(filePathToWrite)
            }
            
            downloadTask.observe(.progress) { (snapshot) in
                let percentComplete = 100.0*Float(snapshot.progress!.completedUnitCount)/Float(snapshot.progress!.totalUnitCount)
                progress?(percentComplete)
            }
            
            return downloadTask
            
//            storageRef.write(toFile: filePathToWrite) { (url, error) in
//
//                if let error = error {
//                    print(error.localizedDescription)
//                }
//                completedHandler?(filePathToWrite)
//            }.observe(.progress) { (snapshot) in
//                let percentComplete = 100.0*Float(snapshot.progress!.completedUnitCount)/Float(snapshot.progress!.totalUnitCount)
//                progress?(percentComplete)
//            }
        }
        else {
            completedHandler?(filePathToWrite)
        }
        
        return nil
    }
    
    class func showProgress(_ view: UIView) {
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = ""
    }
    
    class func showProgressWithPersents(_ view: UIView) -> ((Float) -> Void) {
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = .determinateHorizontalBar
        
        let progressClosure = { (progress: Float) in
            hud.progress = progress / 100.0 // from 0.0 to 1.0
        }
        
        return progressClosure
    }
    
    class func showProgress(_ parent: UIViewController, text: String = "") {
        
        let hud = MBProgressHUD.showAdded(to: parent.view, animated: true)
        hud.label.text = text
    }
    
    class func showProgressWithPersents(_ parent: UIViewController) -> (((Float) -> Void), UILabel) {
        
        let hud = MBProgressHUD.showAdded(to: parent.view, animated: true)
        hud.mode = .determinateHorizontalBar
        
        let progressClosure = { (progress: Float) in
            hud.progress = progress / 100.0 // from 0.0 to 1.0
        }
        
        return (progressClosure, hud.label)
    }
    
    class func hideProgress(_ parent: UIViewController) {
        
        MBProgressHUD.hide(for: parent.view, animated: false)
//        MBProgressHUD.hideAllHUDs(for: parent.view, animated: false)
    }
    
    class func hideProgress(_ view: UIView) {
        
        MBProgressHUD.hide(for: view, animated: false)
//        MBProgressHUD.hideAllHUDs(for: view, animated: false)
    }
    
    class func showInfo(_ text: String, above: UIViewController? = nil, completed: (() -> Void)? = nil) {
        
        var infoController: InfoViewController!
        if above == nil {
            guard let controller = Utils.appDelegate().sceneWindow?.rootViewController?.presentedViewController else {
                return
            }
            infoController = controller.storyboard!.instantiateViewController(withIdentifier: "infoViewController") as? InfoViewController
            infoController.info = text
            infoController.show(above: controller, completion: nil)
        }
        else {
            infoController = above!.storyboard!.instantiateViewController(withIdentifier: "infoViewController") as? InfoViewController
            infoController.info = text
            infoController.show(above: above, completion: nil)
        }
        
        infoController.closeHandler = {
            completed?()
        }
    }
    
    class func showError(_ text: String, above: UIViewController? = nil, completed: (() -> Void)? = nil) {
        
        var errorController: InfoViewController!
        if above == nil {
            guard let controller = Utils.appDelegate().sceneWindow?.rootViewController?.presentedViewController else {
                return
            }
            errorController = controller.storyboard!.instantiateViewController(withIdentifier: "infoViewController") as? InfoViewController
            errorController.error = text
            errorController.show(above: controller, completion: nil)
        }
        else {
            errorController = above!.storyboard!.instantiateViewController(withIdentifier: "infoViewController") as? InfoViewController
            errorController.error = text
            errorController.show(above: above, completion: nil)
        }
        
        errorController.closeHandler = {
            completed?()
        }
    }
    
    class func showErrorIfNeeded(_ error: Error!, above: UIViewController? = nil) {
        if error == nil {
            return
        }
        
        showError(error.localizedDescription, above: above)
    }
    
    class func showConfirm(_ text: String, yesText: String = "YES_STRING".localized, cancelText: String = "CANCEL_STRING".localized, above: UIViewController? = nil, completed: ((_ confirm: Bool) -> Void)?) {
        
        var confirmController: ConfirmViewController!
        if above == nil {
            guard let controller = Utils.appDelegate().sceneWindow?.rootViewController?.presentedViewController else {
                return
            }
            
            confirmController = controller.storyboard!.instantiateViewController(withIdentifier: "confirmViewController") as? ConfirmViewController
            confirmController.info = text
            confirmController.okText = yesText
            confirmController.cancelText = cancelText
            confirmController.show(above: controller, completion: nil)
        }
        else {
            confirmController = above!.storyboard!.instantiateViewController(withIdentifier: "confirmViewController") as? ConfirmViewController
            confirmController.info = text
            confirmController.okText = yesText
            confirmController.cancelText = cancelText
            confirmController.show(above: above, completion: nil)
        }
        
        confirmController.okHandler = {
            completed?(true)
        }
        
        confirmController.cancelHandler = {
            
            completed?(false)
        }
    }
    
    class func colorByTag(_ tag: Int) -> UIColor {
        
        return UIColor.fromInt(intValue: tag)
    }
    
    class func degreesToRadians(_ degrees: CGFloat) -> CGFloat {
        return CGFloat.pi*degrees/180.0
    }
    
    // Don't forget to import AVKit
    class func encodeVideo(at videoURL: URL, completionHandler: ((URL?, Error?) -> Void)?)  {
        
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        let startDate = Date()
        
        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough) else {
            completionHandler?(nil, nil)
            return
        }
        
        //Creating temp path to save the converted video
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        let filePath = documentsDirectory.appendingPathComponent("rendered-Video.mp4")
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(at: filePath)
            } catch {
                completionHandler?(nil, error)
            }
        }
        
        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession.timeRange = range
        
        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession.status {
            case .failed:
                print(exportSession.error ?? "NO ERROR")
                DispatchQueue.main.async {
                    completionHandler?(nil, exportSession.error)
                }
            case .cancelled:
                print("Export canceled")
                DispatchQueue.main.async {
                    completionHandler?(nil, nil)
                }
            case .completed:
                //Video conversion finished
                let endDate = Date()
                
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession.outputURL ?? "NO OUTPUT URL")
                DispatchQueue.main.async {
                    completionHandler?(exportSession.outputURL, nil)
                }
                
            default: break
            }
            
        })
    }
    
    class func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    class var HasAnimations: Bool {
        
        get {
            
            let url = "\(Utils.getDocumentsDirectory().path)/anim 03 - doubt/"
            
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: url)
                return files.filter({ $0.contains("anim-03-") }).count > 0
            }
            catch {
            }
            
            return false
        }
    }
    
    class func showIconBadge(_ number: Int) {
        
        UNUserNotificationCenter.current().requestAuthorization(options: .badge) { (granted, error) in
            
            if error == nil {
                // success
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber = number
                }
            }
        }
    }
}

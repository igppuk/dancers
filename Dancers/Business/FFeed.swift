//
//  FFeed.swift
//  Dancers
//
//  Created by MacOS on 27.06.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import Foundation
import UIKit

class FTextPart: FProtocol, Codable {
    
    var Id = ""
    var Text = ""
    var Image: FImage!
    var Location = 0
    var Length = 0
    
    enum CodingKeys: String, CodingKey {
        
        case Text
        case Image
        case Location
        case Length
    }
    
    required init(from decoder: Decoder) throws {
        
        self.Image = FSerializer.deserialize(FImage.self, [:])
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Text) {
            self.Text = try container.decode(String.self, forKey: .Text)
        }
        if container.contains(.Image) {
            self.Image = try container.decode(FImage.self, forKey: .Image)
        }
        if container.contains(.Location) {
            self.Location = try container.decode(Int.self, forKey: .Location)
        }
        if container.contains(.Length) {
            self.Length = try container.decode(Int.self, forKey: .Length)
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
}

class FFeedEntry: FProtocol, Codable {
    
    var Id = ""
    var Status: EStatus = .unknown
    var Owner: String! = nil
    var To: String!
    var Parent: FFeedEntry! = nil
    var Text = [FTextPart]()
    var Replies = [String: FFeedEntry]()
    var Read = [String: Bool]()
    var Archived = [String: Bool]()
    var Created = Date()
    
    enum CodingKeys: String, CodingKey {
        
        case Status
        case Owner
        case To
        case Parent
        case Text
        case Read
        case Archived
        case Created
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Status) {
            self.Status = try container.decode(EStatus.self, forKey: .Status)
        }
        if container.contains(.Owner) {
            self.Owner = try container.decode(String.self, forKey: .Owner)
        }
        if container.contains(.To) {
            self.To = try container.decode(String.self, forKey: .To)
        }
        if container.contains(.Parent) {
            self.Parent = try container.decode(FFeedEntry.self, forKey: .Parent)
        }
        if container.contains(.Text) {
            self.Text = try container.decode([FTextPart].self, forKey: .Text)
        }
        if container.contains(.Read) {
            self.Read = try container.decode([String: Bool].self, forKey: .Read)
        }
        if container.contains(.Archived) {
            self.Archived = try container.decode([String: Bool].self, forKey: .Archived)
        }
        if container.contains(.Created) {
            if let dateString = try? container.decode(String.self, forKey: .Created) {
                self.Created = try dateString.decodeDate(container, forKey: .Created)
            }
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
}

class FFeed: FProtocol, Codable {
    
    var Id = ""
    var ClubId: String! = nil
    var Entries = [String: FFeedEntry]()
    var Created = Date()
    
    var SortedEntries: [FFeedEntry] {
        let sorted = self.Entries.map({ $0.value }).sorted { (f, s) -> Bool in f.Created > s.Created }
        return sorted
    }
    
    enum CodingKeys: String, CodingKey {
        
        case ClubId
        case Entries
        case Created
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.ClubId) {
            self.ClubId = try container.decode(String.self, forKey: .ClubId)
        }
        if container.contains(.Entries) {
            self.Entries = try container.decode([String: FFeedEntry].self, forKey: .Entries)
        }
        if container.contains(.Created) {
            if let dateString = try? container.decode(String.self, forKey: .Created) {
                self.Created = try dateString.decodeDate(container, forKey: .Created)
            }
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
}

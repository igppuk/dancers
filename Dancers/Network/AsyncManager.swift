//
//  UploadManager.swift
//  Pocket.CEO
//
//  Created by MacOS on 22.04.2020.
//  Copyright © 2020 Vladimir Lozhnikov. All rights reserved.
//

import Foundation
import Firebase
import SwiftyTimer

class AsyncOperation: Operation {
    
    // MARK: Property
    
    var body: INetworkable!
    
    // MARK: Closure
    var completedHandler: ((Error?) -> Void)?
    
    // MARK: Override
    
    override var isAsynchronous: Bool {
        return true
    }
    
    var _isFinished = false
    override var isFinished: Bool {
        set {
            willChangeValue(forKey: "isFinished")
            _isFinished = newValue
            didChangeValue(forKey: "isFinished")
        }
        get {
            return _isFinished
        }
    }
    
    var _isExecuting = false
    override var isExecuting: Bool {
        set {
            willChangeValue(forKey: "isExecuting")
            _isExecuting = newValue
            didChangeValue(forKey: "isExecuting")
        }
        get {
            return _isExecuting
        }
    }
    
    // MARK: Closures
    
    func execute() {
        self.body.asyncNetworkAction { [unowned self] (error) in
            
            self.completedHandler?(error)
        }
    }
    
    override func start() {
        self.isExecuting = true
        execute()
    }
    
    // MARK: Methods
    
    deinit {
        print("asyncoperation \(self.body.Name) \(self.body.UniqueId) deinit")
        self.completedHandler = nil
    }
}

class AsyncManager {
    
    // MARK: Properties
    
    private var operations = [AsyncOperation]()
    private var queue = OperationQueue()
    private var timer: Timer!
    private let timerInterval = 0.7
    
    var maximumParallels: Int {
        get {
            return self.queue.maxConcurrentOperationCount
        }
        set {
            self.queue.maxConcurrentOperationCount = newValue
        }
    }
    
    // MARK: Closures
    
    var progressHandler: ((_ operation: AsyncOperation, _ progress: Float) -> Void)?
    var completedHanlder: ((_ operation: AsyncOperation) -> Void)?
    
    // MARK: init
    
    init() {
        /*!!!Timer.every(5.0) { [unowned self] in
            self.clearObjects()
        }*/
    }
    
    // MARK: Methods
    
    func addObject(_ object: INetworkable, _ completed: ((Error?) -> Void)?) {
        
        if let _ = self.operations.first(where: { $0.body.UniqueId == object.UniqueId }) {
            completed?(nil)
            return
        }
        
        let operation = AsyncOperation()
        operation.body = object
        
        operation.completedHandler = { (error) in
            completed?(error)
        }
        
        self.operations.append(operation)
        self.queue.addOperation(operation)
        
        if self.timer == nil {
            self.timer = Timer.every(self.timerInterval, { [unowned self] in
                
                let inprogress = self.operations.filter({ !$0.isFinished })
                inprogress.forEach({
                    self.progressHandler?($0, $0.body.currentProgress)
                    
                    if $0.body.currentProgress >= 100.0 {
                        $0.isExecuting = false
                        $0.isFinished = true
                        
                        self.completedHanlder?($0)
                    }
                })
            })
        }
    }
    
    func clearObjects() {
        
        self.operations.removeAll { (op) -> Bool in
            if op.body.currentProgress >= 100.0 || op.isFinished || op.isCancelled {
                return true
            }
            
            return false
        }
        
        if self.operations.count == 0 && self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
            print("async timer is stopped")
        }
    }
}

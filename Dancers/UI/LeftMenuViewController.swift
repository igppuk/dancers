//
//  LeftMenuViewController.swift
//  Dancers
//
//  Created by MacOS on 27.06.2020.
//  Copyright © 2020 ciferton. All rights reserved.
//

import UIKit

class LeftMenuViewCell: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var SubscribeLabel: UILabel!
    @IBOutlet weak var SelectImageView: UIImageView!
    @IBOutlet weak var BadgeLabel: UILabel!
    
    // MARK: Closures
    
    // MARK: Properties
    
    weak var Club: FClub!
    
    // MARK: Methods
    
    func show() {
        
        let me = FManager.manager().me!
        self.NameLabel.text = self.Club.Name
        self.SelectImageView.image = self.Club.Selected[me.Phone] == true ? UIImage(named: "done") : nil
        Utils.setImageFromCachOrDownload(self.Club.Image.Url, imageView: self.ImageView, completedHandler: nil)
        
        self.BadgeLabel.isHidden = true
        self.SubscribeLabel.isHidden = true
        if self.Club.Selected[me.Phone] == true {
            let all = self.Club.Feed.Entries.map({ $0.value })
            let read = all.filter({ $0.Read[me.Phone] == true })
            let unreadCount = all.count - read.count
            self.BadgeLabel.text = "\(unreadCount)"
            self.BadgeLabel.isHidden = unreadCount == 0
            self.SubscribeLabel.isHidden = false
        }
    }
    
    // MARK: Actions
}

class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, BecomeAppProtocol {
    
    // MARK: Outlets
    
    @IBOutlet weak var CloseView: UIView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var VersionLabel: UILabel!
    
    // MARK: Closures
    
    var presentHandler: (() -> Void)?
    var dismissHandler: (() -> Void)?
    
    // MARK: Properties
    
    var data = [FClub]()
    var tapOutsideRecogniser: UITapGestureRecognizer!
    
    // MARK: UIViewControllerMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") {
            if let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") {
                self.VersionLabel.text = "version: \(version) (build: \(build))"
            }
        }
        
        self.CloseView.dropShadow()
        refresh()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Utils.appDelegate().becomeView = self
        
        if self.tapOutsideRecogniser == nil {
            self.tapOutsideRecogniser = UITapGestureRecognizer(target: self, action: #selector(handleTapBehind(_:)))
            self.tapOutsideRecogniser.numberOfTapsRequired = 1
            self.tapOutsideRecogniser.cancelsTouchesInView = false
            self.view.window?.addGestureRecognizer(self.tapOutsideRecogniser)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.tapOutsideRecogniser != nil {
            self.view.window?.removeGestureRecognizer(self.tapOutsideRecogniser)
            self.tapOutsideRecogniser = nil
        }
    }
    
    // MARK: BecomeAppProtocol
    
    func become(_ changes: [FProtocol.Type], _ value: Any?) {
        if changes.contains(where: { $0 == FClub.self }) {
            self.refresh()
        }
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.data.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let me = FManager.manager().me!
        let club = self.data[indexPath.row]
        let selected = club.Selected[me.Phone] ?? false
        club.Selected[me.Phone] = !selected
        
        Utils.showProgress(self)
        club.save { (error) in
            Utils.hideProgress(self)
            Utils.showErrorIfNeeded(error)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuViewCell", for: indexPath) as? LeftMenuViewCell else {
            fatalError("wrong cell")
        }
        
        let club = self.data[indexPath.row]
        cell.Club = club
        cell.show()
        
        return cell
    }
    
    // MARK: Methods
    
    func refresh() {
        
        let me = FManager.manager().me!
        self.data.removeAll()
        
        let clubs = (me.MyClubs + me.Clubs).sorted { (f, s) -> Bool in f.Name < s.Name }
        self.data.append(contentsOf: clubs)
        self.TableView.reloadData()
    }
    
    // MARK: Actions
    
    @objc func handleTapBehind(_ recognizer: UIGestureRecognizer) {
        
        if recognizer.state == .ended {
            
            let location = recognizer.location(in: nil)
            let point = self.view.convert(location, from: self.view.window)
            if !self.view.point(inside: point, with: nil) {
                
                dismiss(animated: true) { [unowned self] in
                    self.dismissHandler?()
                }
            }
        }
    }
    
    @IBAction func swipeAction(_ recognizer: UISwipeGestureRecognizer) {
        
        dismiss(animated: true) { [unowned self] in
            self.dismissHandler?()
        }
    }
    
    @IBAction func closeClicked(_ sender: AnyObject) {
        
        dismiss(animated: true) { [unowned self] in
            self.dismissHandler?()
        }
    }
}

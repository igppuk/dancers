//
//  BecomeAppProtocol.swift
//  Pocket.CEO
//
//  Created by Владимир on 01.04.2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import Foundation

protocol BecomeAppProtocol {
    
    func become(_ changes: [FProtocol.Type], _ value: Any?)
}

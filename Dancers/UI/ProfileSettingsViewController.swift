//
//  ProfileSettingsViewController.swift
//  Pocket.CEO
//
//  Created by Владимир on 02.04.2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import UIKit

class ProfileSettingsViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var FirstNameView: UIView!
    @IBOutlet weak var FirstNameTextField: UITextField!
    @IBOutlet weak var LastNameView: UIView!
    @IBOutlet weak var LastNameTextField: UITextField!
    @IBOutlet weak var ProfileImageView: UIImageView!
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var DeleteButton: UIButton!
    
    // MARK: Properties
    
    let imagePicker = UIImagePickerController()
        
    // MARK: Closures
    
    var closeHandler: (() -> Void)?
    
    // MARK: Controller's method
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        self.Step1View.layer.sublayers?.first?.frame = self.Step1View.frame
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
                
        self.FirstNameView.makeShadow()
        self.LastNameView.makeShadow()
        
        self.imagePicker.delegate = self
        
        refresh()
        
//        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: OperationQueue.main) { (notification: Notification) in
//
//           self.ScrollView.setContentOffset(.zero, animated: true)
//        }
        
        let modelName = UIDevice().modelName.lowercased().replace(" ", "")
        if modelName.contains("iphone5") {
            self.ScrollView.isScrollEnabled = true
            self.SaveButton.frame.origin.y = self.LastNameView.frame.maxY + 20.0
            self.DeleteButton.frame.origin.y = self.SaveButton.frame.maxY + 20.0
        }
        else {
            self.DeleteButton.frame.origin.y = self.ScrollView.bounds.height - 100.0
            self.SaveButton.frame.origin.y = self.DeleteButton.frame.minY - self.SaveButton.bounds.height - 20.0
        }
        self.ScrollView.contentSize = CGSize(width: self.ScrollView.bounds.width, height: self.DeleteButton.frame.maxY + 50.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: Methods
    
    func refresh() {
        let me = FManager.manager().me!
        self.DeleteButton.isHidden = me.IsEmpty
        
        // init fields
        self.FirstNameTextField.text = me.FirstName
        self.LastNameTextField.text = me.LastName
        
        Utils.setImageFromCachOrDownload(me.Image.Url, imageView: self.ProfileImageView, completedHandler: nil)
    }
    
    func hideKeyboard() {
        self.FirstNameTextField.resignFirstResponder()
        self.LastNameTextField.resignFirstResponder()
        
        self.ScrollView.setContentOffset(.zero, animated: true)
    }
    
    func save(_ completed: (() -> Void)?) {
        
        let me = FManager.manager().me!
        
        if let firstName = self.FirstNameTextField.text {
            me.FirstName = firstName.trim()
        }
        if let lastName = self.LastNameTextField.text {
            me.LastName = lastName.trim()
        }
        
        Utils.showProgress(self)
        
        me.Device.Model = UIDevice().modelName
        me.Device.Udid = Utils.appDelegate().udid ?? me.Device.Udid
        
        me.save { [unowned self] (error) in
            Utils.hideProgress(self)
            Utils.showErrorIfNeeded(error, above: self)
            
            completed?()
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        
        dismiss(animated: true, completion: { [unowned self] in
            
            /*!!!guard let imageController = self.storyboard!.instantiateViewController(withIdentifier: "processImageViewController") as? ProcessImageViewController else {
                Utils.showError("INTERNAL_ERROR_STRING".localized)
                return
            }*/
            
            let fixedImage = pickedImage.fixedOrientation()
            /*!!!imageController.image = fixedImage ?? pickedImage
            self.present(imageController, animated: true)
            
            imageController.closeHandler = { (image) in
                
                imageController.dismiss(animated: true, completion: nil)*/
                
                guard let image = /*image*/fixedImage else {
                    return
                }
            
                self.ProfileImageView.image = image
                self.ProfileImageView.clearError(okColor: UIColor.clear)
                
                let me = FManager.manager().me!
                me.Image.Image = pickedImage
            //!!!}
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let contentOffset = CGPoint(x: 0.0, y: textField.superview!.frame.minY - 90.0)
        self.ScrollView.setContentOffset(contentOffset, animated: true)
    }
    
    // MARK: Actions
    
    @IBAction func firstNameChanged(_ sender: Any) {
        
        if let t = self.FirstNameTextField.text, t.count >= 1 {
            self.FirstNameTextField.clearError()
        }
    }
    
    @IBAction func lastNameChanged(_ sender: Any) {
        
        if let t = self.LastNameTextField.text, t.count >= 1 {
            self.LastNameTextField.clearError()
        }
    }
    
    @IBAction func tapClicked(_ recognizer: UITapGestureRecognizer) {
        
        // checking for appearence of the keyboard
        if self.FirstNameTextField.isFirstResponder || self.LastNameTextField.isFirstResponder {
            
            hideKeyboard()
            return
        }
        
        let tapPoint = recognizer.location(in: self.ScrollView)
        if self.ProfileImageView.frame.contains(tapPoint) {
            
            self.imagePicker.allowsEditing = false
            
            Utils.showConfirm("SELECT_MEDIA_SOURCE_STRING".localized, yesText: "CAMERA_STRING".localized, cancelText: "PHOTO_LIBRARY_STRING".localized, above: self) { [unowned self] (camera) in
                
                #if targetEnvironment(simulator)
                self.imagePicker.sourceType = .savedPhotosAlbum
                #else
                self.imagePicker.sourceType = camera ? .camera : .savedPhotosAlbum
                
                if camera {
                    self.imagePicker.cameraDevice = .front
                }
                #endif
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func saveClicked(_ sender: AnyObject) {
        
        let me = FManager.manager().me!
        hideKeyboard()
        
        self.ProfileImageView.clearError(okColor: UIColor.clear)
        if !self.FirstNameTextField.validate(errorText: "ENTER_FIRSTNAME_STRING".localized) {
            self.FirstNameTextField.text = nil
            Utils.showError("ENTER_FIRSTNAME_STRING".localized, above: self)
            return
        }
        if !self.LastNameTextField.validate(errorText: "ENTER_LASTNAME_STRING".localized) {
            self.LastNameTextField.text = nil
            Utils.showError("ENTER_LASTNAME_STRING".localized, above: self)
            return
        }
        if me.Image.IsEmpty {
            self.ProfileImageView.showError()
            Utils.showError("ENTER_IMAGE_STRING".localized, above: self)
            return
        }
        
        save { [unowned self] in
            
            self.closeHandler?()
        }
    }
    
    @IBAction func closeClicked(_ sender: AnyObject) {
        
         self.closeHandler?()
    }
    
    @IBAction func deleteAccountClicked(_ sender: AnyObject) {
        
        Utils.showConfirm("DELETE_ACCOUNT_STRING".localized, above: self) { (yes) in
            if yes {
                Utils.appDelegate().becomeView = nil
                FManager.manager().deleteAccount {
                    
                    Utils.appDelegate().showLoginForm()
                }
            }
        }
    }
}

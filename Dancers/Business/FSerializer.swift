//
//  FSerializer.swift
//  Pazitive
//
//  Created by Владимир on 22.01.2019.
//  Copyright © 2019 ciferton. All rights reserved.
//

import Foundation

class FSerializer {
    
    class func deserialize<T>(_ object: T.Type, _ json: [String: Any], _ id: String = "") -> T! where T : FProtocol & Decodable {
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.firebaseDateFormatter)
            
            let fobject = try! decoder.decode(object, from: jsonData)
            
            fobject.Id = id
            fobject.syncChildrens?(id)
            
            return fobject
        }
        catch {
            print(error)
        }
        
        return nil
    }
    
    class func serialize<T>(_ object: T) throws -> [String: Any] where T: Encodable {
        
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(DateFormatter.firebaseDateFormatter)
        
        let jsonData = try encoder.encode(object)
        let encoded = try JSONSerialization.jsonObject(with: jsonData)
        if let encoded = encoded as? [String : Any] {
            return encoded
        }
        
        return [:]
    }
}

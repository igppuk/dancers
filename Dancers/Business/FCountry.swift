//
//  FCountry.swift
//  Pazitive
//
//  Created by Владимир on 19.06.2018.
//  Copyright © 2018 ciferton. All rights reserved.
//

import FirebaseUI

class FCache: FProtocol, Codable {
    
    var Id = ""
    var Subscribers: [String: [String]] = [:] // club: phones
    
    enum CodingKeys: String, CodingKey {
        
        case Subscribers
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Subscribers) {
            self.Subscribers = try container.decode([String: [String]].self, forKey: .Subscribers)
        }
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
    
    // MARK: Methods
}


class FCountry: FProtocol, Codable {
    
    var Id = ""
    var Code = ""
    var Name = ""
    var DisplayName = ""
    var Cache: FCache!
    var Flag: String!
    
    enum CodingKeys: String, CodingKey {
        
        case Code
        case Name
        case DisplayName
        case Cache
    }
    
    required init(from decoder: Decoder) throws {
        
        self.Cache = FSerializer.deserialize(FCache.self, [:])
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Code) {
            self.Code = try container.decode(String.self, forKey: .Code)
        }
        if container.contains(.Name) {
            self.Name = try container.decode(String.self, forKey: .Name)
        }
        if container.contains(.DisplayName) {
            self.DisplayName = try container.decode(String.self, forKey: .DisplayName)
        }
        if container.contains(.Cache) {
            self.Cache = try container.decode(FCache.self, forKey: .Cache)
        }
    }
    
    // MARK: FProtocol
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
    
    func syncChildrens(_ id: String) {
    }
    
    func save(completed: ((Error?) -> Void)?) {
        
        let ref = Database.database().reference()
        let query = ref.child("Countries").child(self.Id)
        
        // update filter
        
        query.updateChildValues(serialize(), withCompletionBlock: { (error, dbref) in
            
            completed?(error)
        })
    }
    
    func saveCach(completed: ((Error?) -> Void)?) {
        
        if let _ = self.Cache {
            
            let ref = Database.database().reference()
            
            // update cahce
            ref.child("Countries").child(self.Id).child("Cache").updateChildValues(self.Cache.serialize(), withCompletionBlock: { (error, dbref) in
                
                completed?(error)
            })
        }
        else {
            completed?(nil)
        }
    }
    
    // MARK: Methods
    
    func getFormattedCodeCountry() -> String {
        if Code == "1" {
            return "(+1)       \(Name)"
        }
        if Code == "61" {
            return "(+61)     \(Name)"
        }
        if Code == "375" {
            return "(+375)   \(Name)"
        }
        if Code == "44" {
            return "(+44)     \(Name)"
        }
        if Code == "99" {
            return "(+99)     \(Name)"
        }
        
        return String()
    }
}

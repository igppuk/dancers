//
//  FImage.swift
//  Pazitive
//
//  Created by Владимир on 19.06.2018.
//  Copyright © 2018 ciferton. All rights reserved.
//

import Foundation
import UIKit

class FImage: FProtocol, Codable {
    
    var Id = ""
    var Name = ""
    var Url = ""
    var ThumbnailName = ""
    var ThumbnailUrl = ""
    var IsVideo = false
    var Created = Date()
    
    var fileUrl: URL! = nil
    var fileThumbnailUrl: URL! = nil
    
    var uploadOperation: INetworkable! = nil
    var isOperationsRemoved = false
    weak var Club: FClub!
    
    var tempImageData: Data!
    var Image: UIImage? {
        set {
            if let data = newValue?.jpegData(compressionQuality: 1.0) {
                self.tempImageData = nil
                self.tempImageData = data
            }
        }
        get {
            if self.tempImageData != nil {
                return UIImage(data: self.tempImageData)
            }
            return nil
        }
    }
    
    var IsEmpty: Bool {
        get {
            return self.Url.isEmpty && self.tempImageData == nil
        }
    }
    
    enum CodingKeys: String, CodingKey {
        
        case Name
        case Url
        case ThumbnailName
        case ThumbnailUrl
        case IsVideo
        case Created
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.Name) {
            do {
                self.Name = try container.decode(String.self, forKey: .Name)
            }
            catch {
                print(error)
            }
        }
        if container.contains(.Url) {
            self.Url = try container.decode(String.self, forKey: .Url)
        }
        if container.contains(.ThumbnailName) {
            do {
                self.ThumbnailName = try container.decode(String.self, forKey: .ThumbnailName)
            }
            catch {
                print(error)
            }
        }
        if container.contains(.ThumbnailUrl) {
            do {
                self.ThumbnailUrl = try container.decode(String.self, forKey: .ThumbnailUrl)
            }
            catch {
                print(error)
            }
        }
        if container.contains(.IsVideo) {
            self.IsVideo = try container.decode(Bool.self, forKey: .IsVideo)
        }
        if container.contains(.Created) {
            if let dateString = try? container.decode(String.self, forKey: .Created) {
                self.Created = try dateString.decodeDate(container, forKey: .Created)
            }
        }
    }
    
    deinit {
        print("image \(self.Id) deinit")
        removeOperations()
    }
    
    func serialize() -> [String: Any] {
        do {
            let encode = try FSerializer.serialize(self)
            return encode
        }
        catch {
            print(error)
        }
        
        return [:]
    }
    
    // MARK: Methods
    
    func removeOperations() {
        if !self.isOperationsRemoved {
         
            self.uploadOperation?.getImage = nil
            self.uploadOperation?.getClub = nil
            self.isOperationsRemoved = true
        }
    }
}

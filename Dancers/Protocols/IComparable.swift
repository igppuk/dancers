//
//  IComparable.swift
//  Pocket.CEO
//
//  Created by MacOS on 08/10/2019.
//  Copyright © 2019 Vladimir Lozhnikov. All rights reserved.
//

import Foundation

protocol IComparable  {
    
    associatedtype CompareEntry
    func CompareTo(_ other: CompareEntry) -> [FProtocol.Type]
}
